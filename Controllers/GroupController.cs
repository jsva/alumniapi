﻿using alumniAPI.Models;
using alumniAPI.Models.DTO.Group;
using alumniAPI.Models.DTO.RequestOptions;
using alumniAPI.Services.GroupService;
using alumniAPI.Services.RequestOptionService;
using alumniAPI.Services.TokenService;
using alumniAPI.Services.UserServices;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace alumniAPI.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    [Route("api/[controller]")]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class GroupController : ControllerBase
    {
        private readonly IGroupService _groupService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly ITokenService _tokenService;
        private readonly IRequestOptionService _requestOptionService;

        public GroupController(IGroupService service, IMapper mapper, IUserService userService, ITokenService tokenService,
            IRequestOptionService requestOptionService)
        {
            _groupService = service;
            _mapper = mapper;
            _userService = userService;
            _tokenService = tokenService;
            _requestOptionService = requestOptionService;
        }

        /// <summary>
        /// Gets all groups
        /// </summary>
        /// <returns></returns>
        /// Hardcoded user id until keycloak works
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]

        public async Task<ActionResult<IEnumerable<GroupReadDTO>>> GetAllGroups()
        {
            // Gets id from token
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);

            int userId = await _tokenService.GetUserIdFromUsernameAsync(username);

            List<Group> groupHolder = new(); 

            List<Group> groupList = (List<Group>) await _groupService.GetAllGroupsAsync();

            foreach(Group gr in groupList)
            {
                // Check if user is member
                if (gr.isPrivate)
                {
                    if (_groupService.IsGroupMember(gr, userId))
                    {
                        // Member
                        groupHolder.Add(gr);
                    }
                }
                else
                {
                    groupHolder.Add(gr);
                }
            }

            var groups = _mapper.Map<List<GroupReadDTO>>(groupHolder);

            GroupTopicRequestOptionsDTO requestOptions = _requestOptionService.ExtractGroupTopicRequestsFromHeader(Request.Headers);
            if (_requestOptionService.RequestOptionsExist(requestOptions))
            {
                groups = _groupService.HandleRequestOptions(groups, requestOptions).ToList();
            }

            return Ok(groups);
        }


        /// <summary>
        /// Creates a new group and adds the creator as its first member
        /// </summary>
        /// <param name="dtpGroup"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]

        public async Task<ActionResult<GroupReadDTO>> AddGroup([FromBody]GroupCreateDTO dtpGroup)
        {
            // Gets id from token
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);

            int userId = await _tokenService.GetUserIdFromUsernameAsync(username);

            Group domainGroup = _mapper.Map<Group>(dtpGroup);


            domainGroup = await _groupService.AddGroupAsync(domainGroup);

            // Add creator
            await AddNewMemberToGroup(domainGroup.id, userId);
            

            return CreatedAtAction("GetGroupById", new { id = domainGroup.id }, _mapper.Map<GroupReadDTO>(domainGroup));
        }

        /// <summary>
        /// Gets a group by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GroupReadDTO>> GetGroupById(int id)
        {
            ///TODO: Use token to verify that user is in the group if private
            Group domainGroup = await _groupService.GetGroup(id);

            // Check if exists
            if(domainGroup == null)
            {
                return NotFound();
            }

            // Check if private
            if(domainGroup.isPrivate)
            {
                // Check if member
                // Gets id from token
                var token = await HttpContext.GetTokenAsync("access_token");
                string username = _tokenService.ExtractUsernameFromToken(token);

                int userId = await _tokenService.GetUserIdFromUsernameAsync(username);

                if (!_groupService.IsGroupMember(domainGroup, userId))
                {
                    // Not a member
                    return Forbid();
                }                 
            }
            // Return base
            return Ok(_mapper.Map<GroupReadDTO>(domainGroup));
        }
        /// <summary>
        /// Add a new user to group, userId is optional and can be left out, will default to current logged in user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        //   If you want the current user to join you still need /userId in link e.g.
        //  /api/User/1/Join/userId
        //  is the link you need to use for the current user to join group 1
        //  If you want to add another user (e.g. user 1) to group 1 use:
        // /api/User/1/Join/userId?userId=1
        [HttpPost("{groupId}/Join/userId")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> AddNewMemberToGroup(int groupId, int userId =0)
        {
            ///TODO: If user is adding another user, check that the adder is a member using token
            Group group = await _groupService.GetGroup(groupId);

            // Check if default userid
            if(userId == 0)
            {
                // Set to current logged in user
                // Gets id from token
                var token = await HttpContext.GetTokenAsync("access_token");
                string username = _tokenService.ExtractUsernameFromToken(token);

                userId = await _tokenService.GetUserIdFromUsernameAsync(username);
            }

            if(group == null)
            {
                return NotFound();
            }

            // Check if private
            if (group.isPrivate)
            {

                // Check if member and group size 
                if(group.GroupMembers.Count > 0)
                {
                   if (!_groupService.IsGroupMember(group, userId))
                   {
                    return Forbid();

                   }
                }  
            }
            // Add
            group.GroupMembers.Add(await _userService.GetUserAsync(userId));

            await _groupService.UpdateGroupAsync(group);

            return NoContent();
        }


    }
}
