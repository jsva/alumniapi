﻿using alumniAPI.Enums;
using alumniAPI.Models;
using alumniAPI.Models.Domain;
using alumniAPI.Models.DTO.Post;
using alumniAPI.Models.DTO.Post.Comment;
using alumniAPI.Models.DTO.RequestOptions;
using alumniAPI.Models.DTO.User;
using alumniAPI.Services.EventService;
using alumniAPI.Services.GroupService;
using alumniAPI.Services.PostService;
using alumniAPI.Services.RequestOptionService;
using alumniAPI.Services.TokenService;
using alumniAPI.Services.TopicService;
using alumniAPI.Services.UserServices;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace alumniAPI.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class PostController : ControllerBase
    {
        private readonly IPostService _postService;
        private readonly ITokenService _tokenService;
        private readonly IUserService _userService;
        private readonly IGroupService _groupService;
        private readonly IEventService _eventService;
        private readonly IRequestOptionService _requestOptionService;
        private readonly ITopicService _topicService;
        private readonly IMapper _mapper;
        private readonly AlumniDbContext _context;

        public PostController(IPostService service, IMapper mapper, AlumniDbContext context, ITokenService tokenService, IUserService userService,
            IGroupService groupService, IEventService eventService, IRequestOptionService requestOptionService, ITopicService topicService)
        {
            _postService = service;
            _mapper = mapper;
            _context = context;
            _tokenService = tokenService;
            _userService = userService;
            _groupService = groupService;
            _eventService = eventService;
            _requestOptionService = requestOptionService;
            _topicService = topicService;
        }
        /// <summary>
        /// Get all posts (and their comments)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetAllPosts( )
        {
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);
            int id = await _tokenService.GetUserIdFromUsernameAsync(username);

            IEnumerable<PostReadDTO> posts = await _postService.GetAllPostsForUser(id);

            PostEventRequestOptionsDTO requestOptions = _requestOptionService.ExtractPostEventRequestsFromHeader(Request.Headers);
            if (_requestOptionService.RequestOptionsExist(requestOptions))
            {
                posts = _postService.HandleRequestOptions(posts, requestOptions);
            }
            return Ok(posts);
        }

        /// <summary>
        /// Get post with specific id
        /// for internal use only (?)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<PostReadDTO>> GetPostById(int id)
        {
            return Ok(await _postService.GetPostAsync(id));
        }
        /// <summary>
        /// Add a new Post to the database
        /// </summary>
        /// <param name="postReadDto"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult<PostReadDTO>> AddPost([FromBody] PostPostDTO postPostDto)
        {
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);
            int id = await _tokenService.GetUserIdFromUsernameAsync(username);

            Post domainPost = _mapper.Map<Post>(postPostDto);
            domainPost.userId = id;
            if (!await _postService.UserAllowedToPost(id, domainPost))
            {
                return Forbid();
            }
            
            domainPost = await _postService.AddPostAsync(domainPost);

            PostReadDTO postReadDto = _mapper.Map<PostReadDTO>(domainPost);
            

            return CreatedAtAction("GetPostById", new { id = domainPost.id },postReadDto);
        }
        /// <summary>
        /// Edit the values of a post(text or title)
        /// lastUpdated will be changed autocmatically
        /// </summary>
        /// <param name="postEditDto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult<PostReadDTO>> EditPost(int id, [FromBody] PostEditDTO postEditDto)
        {
            //Verify that post exists
            if (! await _postService.PostExistsAsync(id))
            {
                return NotFound();
            }
            //Verify that the ids match
            if (id != postEditDto.id)
            {
                return BadRequest();
            }
            //verify that the Original poster is the one editing
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);
            PostReadDTO originalPost = await _postService.GetPostAsync(id);
            if (username != originalPost.user.username)
            {
                Forbid();
            }
            await _postService.EditPostAsync(postEditDto);

            return NoContent();
        }

        /// <summary>
        /// Gets all the posts sent as direct messages to the querying user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("user")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetAllPostsToUser()
        {
            
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);
            int id = await _tokenService.GetUserIdFromUsernameAsync(username);

            var posts = await _postService.GetPostsOfTypeByTypesIdAsync(id, PostTypes.USER);

            GroupTopicRequestOptionsDTO requestOptions = _requestOptionService.ExtractGroupTopicRequestsFromHeader(Request.Headers);
            if (_requestOptionService.RequestOptionsExist(requestOptions))
            {
                posts = _postService.HandleRequestOptions(posts, requestOptions);
            }

            return Ok(posts);
        } 

        /// <summary>
        /// Returns all posts the querying user has received from another specified user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("user/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetAllPostsToUserFromUser( int id)
        {

            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);
            int userId = await _tokenService.GetUserIdFromUsernameAsync(username);

            var posts = await _postService.GetAllPostsToUserFromUser(userId, id);

            GroupTopicRequestOptionsDTO requestOptions = _requestOptionService.ExtractGroupTopicRequestsFromHeader(Request.Headers);
            if (_requestOptionService.RequestOptionsExist(requestOptions))
            {
                posts = _postService.HandleRequestOptions(posts, requestOptions);
            }
            return Ok(posts);
        }
        /// <summary>
        /// Gets all posts to a specified group
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("group/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetAllPostsToGroup(int id)
        {
            if (!await _groupService.GroupExists(id))
            {
                return NotFound();
            }
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);
            int userid = await _tokenService.GetUserIdFromUsernameAsync(username);
            if (!await _userService.UserIsMemberOfGroup(userid, id))
            {
                return Forbid();
            }

            var posts = await _postService.GetPostsOfTypeByTypesIdAsync(id, PostTypes.GROUP);

            GroupTopicRequestOptionsDTO requestOptions = _requestOptionService.ExtractGroupTopicRequestsFromHeader(Request.Headers);
            if (_requestOptionService.RequestOptionsExist(requestOptions))
            {
                posts = _postService.HandleRequestOptions(posts, requestOptions);
            }

            return Ok(posts);
        }
        /// <summary>
        /// Gets all posts to a specified event
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("event/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetAllPostsToEvent(int id)
        {
            if (! await _eventService.EventExists(id))
            {
                return NotFound();
            }
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);
            int userid = await _tokenService.GetUserIdFromUsernameAsync(username);
            if (!await _userService.UserIsInvitedToEvent(userid, id))
            {
                return Forbid();
            }

            var posts = await _postService.GetPostsOfTypeByTypesIdAsync(id, PostTypes.EVENT);

            GroupTopicRequestOptionsDTO requestOptions = _requestOptionService.ExtractGroupTopicRequestsFromHeader(Request.Headers);
            if (_requestOptionService.RequestOptionsExist(requestOptions))
            {
                posts = _postService.HandleRequestOptions(posts, requestOptions);
            }

            return Ok(posts);
        }

        /// <summary>
        /// Gets all posts to a specified topic
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("topic/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetAllPostsToTopic(int id)
        {
            
            if (! await _topicService.TopicExistsAsync(id))
            {
                return NotFound();
            }

            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);
            int userid = await _tokenService.GetUserIdFromUsernameAsync(username);

            if (! await _userService.UserIsMemberOfTopic(userid, id))
            {
                return Forbid();
            }

            var posts = await _postService.GetPostsOfTypeByTypesIdAsync(id, PostTypes.TOPIC);

            GroupTopicRequestOptionsDTO requestOptions = _requestOptionService.ExtractGroupTopicRequestsFromHeader(Request.Headers);
            if (_requestOptionService.RequestOptionsExist(requestOptions))
            {
                posts = _postService.HandleRequestOptions(posts, requestOptions);
            }

            return Ok(posts);
        }

        /// <summary>
        /// Get post with specific id
        /// for internal use only (?)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("Comment/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<PostReadDTO>> GetCommentById(int id)
        {
            return Ok(await _postService.GetCommentAsync(id));
        }

        /// <summary>
        /// Post a new comment to the API
        /// </summary>
        /// <param name="commentPostDto"></param>
        /// <returns></returns>
        [HttpPost("Comment")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult<PostReadDTO>> AddComment([FromBody] CommentPostDTO commentPostDto)
        {
            // check if user can see post
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);
            int userid = await _tokenService.GetUserIdFromUsernameAsync(username);

            if (! await _postService.UserAllowedToPost(userid, commentPostDto.postId))
            {
                return Forbid();
            }

            Comment domainComment = _mapper.Map<Comment>(commentPostDto);
            domainComment.userId = userid;
            domainComment = await _postService.AddCommentAsync(domainComment);

            CommentReadDTO commentReadDto = _mapper.Map<CommentReadDTO>(domainComment);
           
            return CreatedAtAction("GetCommentById",new { id = domainComment.id },commentReadDto);
        }
        /// <summary>
        /// Edit a comment in the API
        /// </summary>
        /// <param name="commentEditDto"></param>
        /// <returns></returns>
        [HttpPut("Comment/{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult<PostReadDTO>> EditComment(int id,[FromBody] CommentEditDTO commentEditDto)
        {
            ///TODO: Check if user is the original commenter
            if (!await _postService.CommentExistsAsync(commentEditDto.id))
            {
                return NotFound();
            }
            if (commentEditDto.id !=id )
            {
                return BadRequest();
            }
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);
            CommentReadDTO commentReadDto = await _postService.GetCommentAsync(id);
            if (username != commentReadDto.user.username)
            {
                return Forbid();
            }

            await _postService.EditCommentAsync(commentEditDto);
            return NoContent();
        }
        //TEST METHOD REMOVE LATER
        //[HttpGet("Username/{username}")]
        //public async Task<ActionResult<int>> PostUser(string username)
        //{
        //    int id = await _tokenService.GetUserIdFromUsernameAsync(username);
        //    return Ok(id);
        //}

    }
}
