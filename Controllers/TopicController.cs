﻿using alumniAPI.Models;
using alumniAPI.Models.Domain;
using alumniAPI.Models.DTO.Group;
using alumniAPI.Models.DTO.RequestOptions;
using alumniAPI.Models.DTO.Topic;
using alumniAPI.Services.GroupService;
using alumniAPI.Services.RequestOptionService;
using alumniAPI.Services.TokenService;
using alumniAPI.Services.TopicService;
using alumniAPI.Services.UserServices;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace alumniAPI.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    [Route("api/[controller]")]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class TopicController : ControllerBase
    {
        private readonly ITopicService _topicService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly ITokenService _tokenService;
        private readonly IRequestOptionService _requestOptionService;

        public TopicController(ITopicService topicService, IMapper mapper, IUserService userService, ITokenService tokenService,
            IRequestOptionService requestOptionService)
        {
            _topicService = topicService;
            _mapper = mapper;
            _userService = userService;
            _tokenService = tokenService;
            _requestOptionService = requestOptionService;
        }
        /// <summary>
        /// Get all the topics
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<TopicReadDTO>>> GetAllTopics()
        {
            var topics = _mapper.Map<List<TopicReadDTO>>(await _topicService.GetAllTopicsAsync());

            GroupTopicRequestOptionsDTO requestOptions = _requestOptionService.ExtractGroupTopicRequestsFromHeader(Request.Headers);
            if (_requestOptionService.RequestOptionsExist(requestOptions))
            {
                topics = _topicService.HandleRequestOptions(topics, requestOptions).ToList();
            }
            return Ok(topics);
        }

        /// <summary>
        /// Get a specific topic by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<TopicReadDTO>> GetTopicById(int id)
        {
            if (! await _topicService.TopicExistsAsync(id))
            {
                return NotFound();
            }
            //Doesn't specify if only members can do this
            Topic topic = await _topicService.GetTopicByIdAsync(id);
            TopicReadDTO topicReadDto = _mapper.Map<TopicReadDTO>(topic);
            return Ok(topicReadDto);

        }

        /// <summary>
        /// Add a new topic
        /// </summary>
        /// <param name="topicPostDto"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ActionResult<TopicReadDTO>> AddTopic([FromBody] TopicPostDTO topicPostDto)
        {
            Topic topic = _mapper.Map<Topic>(topicPostDto);
            topic = await _topicService.AddTopicAsync(topic);

            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);

            int userId = await _tokenService.GetUserIdFromUsernameAsync(username);

            await _topicService.InviteUserToTopicAsync(topic.id, userId);

            return CreatedAtAction("GetTopicById", new { id = topic.id }, _mapper.Map<TopicReadDTO>(topic));
        }

        /// <summary>
        /// Adds the requesting user to the membership of the topic corresponding to the provided id
        /// </summary>
        /// <param name="topicId"></param>
        /// <returns></returns>
        [HttpPost("{topicId}/join")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> JoinTopic(int topicId)
        {
            if (! await _topicService.TopicExistsAsync(topicId))
            {
                return NotFound();
            }

            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);

            int userId = await _tokenService.GetUserIdFromUsernameAsync(username);

            await _topicService.InviteUserToTopicAsync(topicId, userId);
            return NoContent();
        }
      


    }
}
