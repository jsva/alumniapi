﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using alumniAPI.Models;
using alumniAPI.Models.DTO.User;
using alumniAPI.Services.UserServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using alumniAPI.Services.TokenService;
using Microsoft.AspNetCore.Authentication;

namespace alumniAPI.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    [Route("api/[controller]")]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]


    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AlumniDbContext _context;
        private readonly ITokenService _tokenService;

        public UserController(IUserService service, IMapper mapper, AlumniDbContext context, ITokenService tokenService)
        {
            _userService = service;
            _mapper = mapper;
            _context = context;
            _tokenService = tokenService;
        }

        /// <summary>
        /// Redirects you to the get /user/id with the correct id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status307TemporaryRedirect)]
        public async Task<ActionResult> GetAllUsers()
        {
            
            ///If user exists: redirect to getuserbyId with their id
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);
            int id = await _tokenService.GetUserIdFromUsernameAsync(username);

            string baseurl = "https://localhost:44366/api/User";
            string url = baseurl + $"/{id}";

            return RedirectPreserveMethod(url);
            //return _mapper.Map<List<UserReadDTO>>(await _userService.GetAllUsersAsync());

        }


        /// <summary>
        /// Create a new user
        /// </summary>
        /// <param name="dtoUser"></param>
        /// <returns></returns>
        /// 
        // NOT IN USE
        //[HttpPost]
        //[ProducesResponseType(StatusCodes.Status201Created)]
        //public async Task<ActionResult<User>> AddUser(UserCreateDTO dtoUser)
        //{
        //    ///TODO: REmove?
        //    User domainUser = _mapper.Map<User>(dtoUser);

        //    domainUser = await _userService.AddUserAsync(domainUser);

        //    return CreatedAtAction("GetUserById",
        //        new { id = domainUser.id },
        //        _mapper.Map<UserReadDTO>(domainUser));
        //}

        /// <summary>
        /// Gets a specific user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<UserReadDTO>> GetUserById(int id)
        {
            ///TODO: Check that id matches with token (may not be what we want)
            User domainUser = await _userService.GetUserAsync(id);

            if(domainUser == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<UserReadDTO>(domainUser));
        }
        /// <summary>
        /// Edits an user
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoUser"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<IActionResult> EditUser(int id, [FromBody] UserEditDTO userEditDto)
        {
            ///TODO: Check that userid and token match
           if (!_userService.UserExists(id))
           {
                return NotFound();
           } 
           if(id != userEditDto.Id)
           {
                return BadRequest();
           }
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);
            int tokenid = await _tokenService.GetUserIdFromUsernameAsync(username);
            if (tokenid != id)
            {
                return Forbid();
            }
            
            await _userService.EditUser(userEditDto);

            return NoContent();
        }

    }
}
