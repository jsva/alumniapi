﻿using alumniAPI.Models;
using alumniAPI.Models.Domain;
using alumniAPI.Models.DTO.Event;
using alumniAPI.Models.DTO.RequestOptions;
using alumniAPI.Services.EventService;
using alumniAPI.Services.GroupService;
using alumniAPI.Services.RequestOptionService;
using alumniAPI.Services.TokenService;
using alumniAPI.Services.TopicService;
using alumniAPI.Services.UserServices;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace alumniAPI.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    [Route("api/[controller]")]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class EventController : ControllerBase
    {
        private readonly IEventService _eventService;
        private readonly IUserService _userService;
        private readonly IGroupService _groupService;
        private readonly IMapper _mapper;
        private readonly AlumniDbContext _context;
        private readonly ITokenService _tokenService;
        private readonly ITopicService _topicService;
        private readonly IRequestOptionService _requestOptionService;

        public EventController(IEventService service, IMapper mapper, IUserService userService, IGroupService groupService,
            AlumniDbContext context, ITokenService tokenService, ITopicService topicService, IRequestOptionService requestOptionService)
        {
            _groupService = groupService;
            _eventService = service;
            _mapper = mapper;
            _context = context;
            _userService = userService;
            _tokenService = tokenService;
            _topicService = topicService;
            _requestOptionService = requestOptionService;
        }

        /// <summary>
        /// Gets all events
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]

        public async Task<ActionResult<IEnumerable<EventReadDTO>>> GetAllEvents()
        {
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);

            int userId = await _tokenService.GetUserIdFromUsernameAsync(username);
            //Get events this user should be able to see
            var events = _mapper.Map<List<EventReadDTO>>(await _eventService.GetEventsForUser(userId));

            PostEventRequestOptionsDTO requestOptions = _requestOptionService.ExtractPostEventRequestsFromHeader(Request.Headers);
            if (_requestOptionService.RequestOptionsExist(requestOptions))
            {
                events = _eventService.HandleRequestOptions(events, requestOptions).ToList();
            }

            return events;
        }

        /// <summary>
        /// Gets an event using id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]

        public async Task<ActionResult<EventReadDTO>> GetEventById(int id)
        {

            if (! await _eventService.EventExists(id))
            {
                return NotFound();
            }
            Event domainEvent = await _eventService.GetEventById(id);
                   
            return Ok(_mapper.Map<EventReadDTO>(domainEvent));
        }

        /// <summary>
        /// Creates a new event
        /// </summary>
        /// <param name="ev"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]

        public async Task<ActionResult<EventReadDTO>> AddEvent([FromBody] EventCreateDTO ev)
        {
            // Gets id from token
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);

            int userId = await _tokenService.GetUserIdFromUsernameAsync(username);

            Event domainEvent = _mapper.Map<Event>(ev);
            domainEvent.lastUpdated = DateTime.Now;
            domainEvent.createdBy = userId;

            domainEvent = await _eventService.AddNewEventAsync(domainEvent);

            EventReadDTO eventReadDto = _mapper.Map<EventReadDTO>(domainEvent);          

            return CreatedAtAction("GetEventById", new { id = domainEvent.id }, eventReadDto);
        }

       
        /// <summary>
        /// Edit an event by using id
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="dtoEvent"></param>
        /// <returns></returns>
        [HttpPut("{eventId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]

        public async Task<IActionResult> EditEvent(int eventId, [FromBody] EventEditDTO dtoEvent)
        {

            // Check if exists
            if (! await _eventService.EventExists(eventId))
            {
                return NotFound();
            }
            if (eventId != dtoEvent.id)
            {
                return BadRequest();
            }

            // Gets id from token
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);

            int userId = await _tokenService.GetUserIdFromUsernameAsync(username);


            // Check if user is event creator(owner)

            if (dtoEvent.createdBy != userId)
            {
                return Forbid();
            }

            if (dtoEvent.createdBy != userId)
            {
                return Forbid();
            }

            Event domainEvent = _mapper.Map<Event>(dtoEvent);

            await _eventService.UpdateEvent(domainEvent);

            return NoContent();
        }

        /// <summary>
        /// Invite a group to an event, only event creator may invite
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpPost("{eventId}/invite/group/{groupId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]

        public async Task<IActionResult> InviteGroupToEvent(int eventId, int groupId)
        {
            // Check if event exists
            if (! await _eventService.EventExists(eventId))            {
                return NotFound();
            }

            // Check if group exists
            if (! await _groupService.GroupExists(groupId))
            {
                return NotFound();
            }
            // Gets id from token
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);

            int userId = await _tokenService.GetUserIdFromUsernameAsync(username);
            Event domainEvent = await _eventService.GetEventById(eventId);
            // Check if user is event creator(owner)
            if (domainEvent.createdBy != userId)
            {
                return Forbid();
            }

            // All is checked, invite group

            await _eventService.InviteGroupToEvent(eventId, groupId);

            return NoContent();
        }

        /// <summary>
        /// Delete a group invite
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpDelete("{eventId}/invite/group/{groupId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteEventGroupInvite(int eventId, int groupId)
        {
            // find event

            if (!await _eventService.EventExists(eventId))
            {
                return NotFound();
            }

            // Find group
            if (! await _groupService.GroupExists(groupId))
            {
                return NotFound();
            }

            
            // Gets id from token
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);

            int userId = await _tokenService.GetUserIdFromUsernameAsync(username);
            Event ev = await _eventService.GetEventById(eventId);
            // Check if user is event owner
            if (ev.createdBy != userId)
            {
                return Forbid();
            }

            await _eventService.DeleteEventGroupInvite(eventId, groupId);

            return NoContent();
        }

        /// <summary>
        /// Invites a single user
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPost("{eventId}/invite/user/{userId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> InviteSingleUser(int eventId, int userId)
        {       
            // Check if user exists
            
            if (! await _eventService.EventExists(eventId))
            {
                return NotFound();
            }

            // Check if user exists
           if (!  _userService.UserExists(userId))
            {
                return NotFound();
            }
            // Gets id from token
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);

            int creatorUserId = await _tokenService.GetUserIdFromUsernameAsync(username);
            Event ev = await _eventService.GetEventById(eventId);
            // Check if user is creator of event
            if (ev.createdBy != creatorUserId)
            {
                return Forbid();
            }

            await _eventService.InviteUserToEvent(eventId, userId);

            return NoContent();
        }

        /// <summary>
        /// Deletes a invite to a single user
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete("{eventId}/invite/user/{userId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteSingleUserInvite(int eventId, int userId)
        {
            // find event
            

            if (!await _eventService.EventExists(eventId))
            {
                return NotFound();
            }

            // Find user
            if (!  _userService.UserExists(userId))
            {
                return NotFound();
            }

            // Gets id from token
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);

            int creatorUserId = await _tokenService.GetUserIdFromUsernameAsync(username);
            Event ev = await _eventService.GetEventById(eventId);

            if (ev.createdBy != creatorUserId)
            {
                return Forbid();
            }

            await _eventService.DeleteEventUserInvite(eventId, userId);

            return NoContent();
        }

        /// <summary>
        /// Invites a topic to an event, only allowed if you are event creator
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="topicId"></param>
        /// <returns></returns>
        [HttpPost("{eventId}/invite/topic/{topicId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> InviteTopicToEvent(int eventId, int topicId)
        {
            // Check if event exists
            if (!await _eventService.EventExists(eventId))
            {
                return NotFound();
            }

            // Check if topic exists
            if (!await _topicService.TopicExistsAsync(topicId))
            {
                return NotFound();
            }
            // Gets id from token
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);

            int userId = await _tokenService.GetUserIdFromUsernameAsync(username);
            Event domainEvent = await _eventService.GetEventById(eventId);
            // Check if user is event creator(owner)
            if (domainEvent.createdBy != userId)
            {
                return Forbid();
            }

            // All is checked, invite topic

            await _eventService.InviteTopicToEvent(eventId, topicId);

            return NoContent();
        }

        /// <summary>
        /// Removes an invite to a topic from an event, only allowed if you are event creator
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="topicId"></param>
        /// <returns></returns>
        [HttpDelete("{eventId}/invite/topic/{topicId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteTopicInvite(int eventId, int topicId)
        {
            // find event
            if (!await _eventService.EventExists(eventId))
            {
                return NotFound();
            }

            // Find topic
            if (! await _topicService.TopicExistsAsync(topicId))
            {
                return NotFound();
            }

            // Gets id from token
            var token = await HttpContext.GetTokenAsync("access_token");
            string username = _tokenService.ExtractUsernameFromToken(token);

            int creatorUserId = await _tokenService.GetUserIdFromUsernameAsync(username);
            Event ev = await _eventService.GetEventById(eventId);

            if (ev.createdBy != creatorUserId)
            {
                return Forbid();
            }

            await _eventService.DeleteEventTopicInvite(eventId, topicId);

            return NoContent();
        }
        /// RSVP NON MVP
    }
}
