﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using alumniAPI.Models.Domain;

namespace alumniAPI.Models
{
    [Table("User")]
    public class User
    {
        [Required]
        public int id { get; set; }

        [Required]
        [MaxLength(40)]
        public string username { get; set; }

        [Url]
        public string img { get; set; }

        [MaxLength(150)]
        public string status { get; set; }

        [MaxLength(300)]
        public string bio { get; set; }

        [MaxLength(150)]
        public string funFact { get; set; }

        public ICollection<Group> Groups { get; set; }
        public ICollection<Topic> Topics { get; set; }
        public ICollection<Event> Events { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<Post>? Posts { get; set; }
        public ICollection<RSVP> RSVP { get; set; }

    }
}
