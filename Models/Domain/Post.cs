﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace alumniAPI.Models.Domain
{
    [Table("Post")]
    public class Post
    {
        [Required]
        public int id { get; set; }
        public DateTime lastUpdated { get; set; }
        public string text { get; set; }
        // FK

        public int? userId { get; set; }
        public User? user { get; set; } //should only be null if user is deleted.

        public string title { get; set; }
        public ICollection<Comment> comments { get; set; }

        // Relations that are just manual FKs, because EFCore doesn't like to have two user relationships
        public int targetGroupId { get; set; }
        public int targetUserId { get; set; }
        public int targetEventId { get; set; }
        public int targetTopicId { get; set; }

    }
}
