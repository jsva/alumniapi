﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace alumniAPI.Models.Domain
{
    [Table("Event")]
    public class Event
    {
        [Required]
        public int id { get; set; }

        public DateTime lastUpdated { get; set; }

        public string name { get; set; }

        // FK
        public int createdBy { get; set; }

        public string description { get; set; }

        public bool allowGuests { get; set; }

        public string bannerImg { get; set; }

        public DateTime startTime { get; set; }
        public DateTime endTime { get; set; }

        public ICollection<RSVP> RSVPList { get; set; }
        public ICollection<User> UserInvited { get; set; }
        public ICollection<Topic> TopicInvited { get; set; }

        public ICollection<Group> GroupsInvited { get; set; }



    }
}
