﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace alumniAPI.Models.Domain
{
    [Table("Topic")]
    public class Topic
    {
        [Required]
        public int id { get; set; }

        public string name { get; set; }

        public string description { get; set; }

        // FK
        public ICollection<User> UsersFollow { get; set; }
        public ICollection<Event> EventInvited { get; set; }

    }
}
