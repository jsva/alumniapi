﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace alumniAPI.Models.Domain
{
    [Table("RSVP")]
    public class RSVP
    {
        public int id { get; set; }
        [ForeignKey("UserFK")]
        public User user { get; set; }
        [ForeignKey("EventFK")]
        public Event eventet { get;set; }

        public DateTime lastUpdated { get; set; }
        public int guestCount { get; set; }
    }
}
