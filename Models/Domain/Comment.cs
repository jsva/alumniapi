﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace alumniAPI.Models.Domain
{
    [Table("Comment")]
    public class Comment
    {
        [Required]
        public int id { get; set; }

        public string text { get; set; }

        public DateTime lastUpdated { get; set; }

        // FK
        [ForeignKey("parentCommentId")]
        public int parentCommentId { get; set; }
        public Comment comment { get; set; }
        [ForeignKey("postId")]
        public int postId { get; set; }
        public Post post { get; set; }
        [ForeignKey("userId")]
        public int userId { get; set; }
        public User user { get; set; }
    }
}
