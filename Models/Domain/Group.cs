﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using alumniAPI.Models.Domain;

namespace alumniAPI.Models
{
    [Table("Group")]
    public class Group
    {

        [Required]
        public int id { get; set; }

        [MaxLength(50)]
        public string name { get; set; }
        public string description { get; set; }
        
        public bool isPrivate {get;set;}

        public ICollection<User> GroupMembers { get; set; }
        public ICollection<Event> EventInvites { get; set; }
        public ICollection<Post> GroupPosts { get; set; }
    }
}
