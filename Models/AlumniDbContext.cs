﻿
using alumniAPI.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace alumniAPI.Models
{
    public class AlumniDbContext :DbContext
    {
        public AlumniDbContext( DbContextOptions options) : base(options)
        {

        }

        public DbSet<Comment> Comments { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<RSVP> RSVPs { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Users
            modelBuilder.Entity<User>().HasData(new User { id = 1, username = "Ronny", img = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJODdqu99Oiu-uDdtc2_bF35X7pczodMocCA&usqp=CAU", status = "Living life", bio = "kedetgåri", funFact = "ja" });
            modelBuilder.Entity<User>().HasData(new User { id = 2, username = "Patrick", img = "https://img.freepik.com/free-photo/cool-man-biker-sunglasses-sitting-near-his-motocycle_93675-86796.jpg?size=626&ext=jpg", status = "Riding along", bio = "Life is like a MC trip. Fun, speed and death around every corner", funFact = "(-■_■)" });
            modelBuilder.Entity<User>().HasData(new User { id = 3, username = "Ivan", img = "https://i.imgflip.com/2mmfa5.jpg", status = "Currently vacationing in Ukraine <3", bio = "Just your average russian life enjoyer", funFact = "My Kalashnikov is not the largest thing i own ;)" });
            modelBuilder.Entity<User>().HasData(new User { id = 4, username = "S.V Indel", img = "idk", status = "holder på med ting", bio = "Låner ting på permanet basis", funFact = "Alt ditt er mitt" });
            modelBuilder.Entity<User>().HasData(new User { id = 5, username = "Bernt Esserwisser", img = "https://christineotterstad.files.wordpress.com/2012/09/besserwissere.jpg", status = "Opplyser befolkningen", bio = "Jeg vet bedre enn deg", funFact = "Jeg er medlem i mensa" });

            // Groups
            modelBuilder.Entity<Group>().HasData(new Group { id = 1, name = "Motorcycle", description = "Group for motorcycle enthusiasts. All things MC", isPrivate = false });
            modelBuilder.Entity<Group>().HasData(new Group { id = 2, name = "Football", description = "The nr 1 place to go for football news and discussions", isPrivate = false });
            modelBuilder.Entity<Group>().HasData(new Group { id = 3, name = "Climbing", description = "For those who aim high", isPrivate = false });
            modelBuilder.Entity<Group>().HasData(new Group { id = 4, name = "Ronny and Friends", description = "Ronny's closest friends", isPrivate = true });

            // Event
            modelBuilder.Entity<Event>().HasData(new Event { id = 1,
                lastUpdated = DateTime.Now, name = "The grand I̶n̶v̶a̶s̶i̶o̶n̶  vacaction trip to Ukraine",
                createdBy= 3, description="Get you AK's and MIG's ready, we are going on our yearly vacation trip to Kyiv!",
                allowGuests=true,
                bannerImg= "https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Flag_of_Ukraine.svg/1200px-Flag_of_Ukraine.svg.png",
                startTime= DateTime.Parse("2022 - 02 - 24"),
                endTime = DateTime.Parse("2030 - 01 - 05")});

            modelBuilder.Entity<Event>().HasData(new Event
            {
                id = 2,
                lastUpdated = DateTime.Now,
                name = "Rideout MC Trip",
                createdBy = 2,
                description = "Prepare for the biggest spring rideout in Norway!",
                allowGuests = true,
                bannerImg = "https://2yrh403fk8vd1hz9ro2n46dd-wpengine.netdna-ssl.com/wp-content/uploads/2017/06/2017-Yamaha-YZF-R3-Review-Motorcycle-14.jpg",
                startTime = DateTime.Parse("2022 - 04 - 10"),
                endTime = DateTime.Parse("2022 - 04 - 10")
            });

            modelBuilder.Entity<Event>().HasData(new Event
            {
                id = 3,
                lastUpdated = DateTime.Now,
                name = "Bring all your valuables",
                createdBy = 4,
                description = "Got someting valueable? Let me take a look and give you an offer!",
                allowGuests = true,
                bannerImg = "https://www.business2community.com/wp-content/uploads/2014/10/stealing-content.png.png",
                startTime = DateTime.Parse("2022 - 05 - 20"),
                endTime = DateTime.Parse("2022 - 05 - 20")
            });

            // Topic
            modelBuilder.Entity<Topic>().HasData(new Topic
            {
                id = 1,
                name = "Russian",
                description = "All things russian/CSGO"
            });

            // Topic
            modelBuilder.Entity<Topic>().HasData(new Topic
            {
                id = 2,
                name = "Motorcycle",
                description = "Imagine an bike, but with an engine instead of your legs"
            });

            //Posts
            modelBuilder.Entity<Post>().HasData(new Post
            {
                id = 1,
                title = "Bruker dokkar hjelm bois?",
                text = "Har solgt hjelmen min for å kunne kjøpe litt mer bensin, så lurte på hvor mange andre som kjørte rundt uten hjelm?",
                lastUpdated = DateTime.Now,
                userId = 2,
                targetGroupId = 1
            });
            modelBuilder.Entity<Post>().HasData(new Post
            {
                id = 2,
                title = "Har billig bensin til salgs!",
                text = "Har noe skikkelig god og billig bensin til salgs. Kan hentes hos meg, betaling på forhånd. 15kr/l. Mvh Svein Vegard Indel ",
                lastUpdated = DateTime.Now,
                userId = 4,
                targetGroupId = 1
            });
            modelBuilder.Entity<Post>().HasData(new Post
            {
                id = 3,
                title = "Looking forward to this!!",
                text = "I am really happy to get to go on a vacation, i hope we will get welcomed as warmly as we have been promised!",
                lastUpdated = DateTime.Now,
                userId = 3,
                targetEventId = 1
            });

            //Comments
            modelBuilder.Entity<Comment>().HasData(new Comment
            {
                id = 1,
                text = "Hei, jeg kjøper 15 000 liter sånn at jeg får kjørt litt hehehe",
                lastUpdated = DateTime.Now,
                postId = 2,
                userId = 2
            });
            modelBuilder.Entity<Comment>().HasData(new Comment
            {
                id = 2,
                text = "Hvor blir denne bensinen av??? Jeg har jo sendt penger men ikke fått bensin!!!!",
                lastUpdated = DateTime.Now,
                postId = 2,
                userId = 2,
                parentCommentId = 1
            });
            modelBuilder.Entity<Comment>().HasData(new Comment
            {
                id = 3,
                text = "Hvis du hadde lest litt nøyere hadde du kanskje innsett at fyren er bokstavelig talt en S V indler, og burde dermed ikke stoles på.",
                lastUpdated = DateTime.Now,
                postId = 2,
                userId = 5,
                parentCommentId = 2
            });
            modelBuilder.Entity<Comment>().HasData(new Comment
            {
                id = 4,
                text = "In russia we normally don't use helm, but when we now vacation we use special helm from 1944, is very good",
                lastUpdated = DateTime.Now,
                postId = 1,
                userId = 3,
             });
            modelBuilder.Entity<Comment>().HasData(new Comment
            {
                id = 5,
                text = "Hvis du trenger ny hjelm for en billig penge så kan jeg selge deg en særdeles god hjelm for en billig penge",
                lastUpdated = DateTime.Now,
                postId = 1,
                userId = 4,
            });
            modelBuilder.Entity<Comment>().HasData(new Comment
            {
                id = 6,
                text = "All forskning viser at å bruke hjelm redder liv, å ikke bruke hjelm er dermed et ekstremt dårlig valg.",
                lastUpdated = DateTime.Now,
                postId = 1,
                userId = 5,
            });
            modelBuilder.Entity<Comment>().HasData(new Comment
            {
                id = 7,
                text = "Kjøper ikke hjelm fra deg før jeg får den jævla bensinen min!",
                lastUpdated = DateTime.Now,
                postId = 1,
                userId = 1,
                parentCommentId = 5
            });


            //FLUENT API RELATIONSHIP DEFINITIONS

            // POST and user
            // Post has one user, user has many posts
            // Relationships defined directly in seeding objects
            modelBuilder.Entity<Post>()
                .HasOne(p => p.user)
                .WithMany(u => u.Posts)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.NoAction);

            // Comment and user
            // Comment has one user, user has many comments
            // Relationships defined directly in seeding objects

            modelBuilder.Entity<Comment>()
                .HasOne(c => c.user)
                .WithMany(u => u.Comments)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.NoAction);

            // Comment and Post
            // Comment has one post, post has many comments
            // Relationships defined directly in seeding objects
            modelBuilder.Entity<Comment>()
                .HasOne(c => c.post)
                .WithMany(p => p.comments)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.NoAction);

            // User and Groups
            // User has many groups, groups have many users
            modelBuilder.Entity<User>()
                .HasMany(u => u.Groups)
                .WithMany(g => g.GroupMembers)
                .UsingEntity<Dictionary<string, object>>(
                "GroupMembers",
                gm => gm.HasOne<Group>().WithMany().HasForeignKey("groupId"),
                gm => gm.HasOne<User>().WithMany().HasForeignKey("userId"),
                table =>
                {
                    table.HasKey("groupId", "userId");
                    table.HasData(
                        new {userId = 1, groupId = 1 },
                        new {userId = 1, groupId = 2 },
                        new {userId = 1, groupId = 3 },
                        new {userId = 2, groupId = 1 },
                        new {userId = 3, groupId = 1 },
                        new {userId = 4, groupId = 1 },
                        new {userId = 5, groupId = 1 },
                        new {userId = 5, groupId = 2 },
                        new {userId = 5, groupId = 3 },
                        new {userId = 5, groupId = 4 }
                        );
                });

            // User and Events
            // User has many events, events has many users
            modelBuilder.Entity<User>()
                .HasMany(u => u.Events)
                .WithMany(e => e.UserInvited)
                .UsingEntity<Dictionary<string, object>>(
                "InvitedUsers",
                iu => iu.HasOne<Event>().WithMany().HasForeignKey("eventId"),
                iu => iu.HasOne<User>().WithMany().HasForeignKey("userId"),
                table =>
                {
                    table.HasKey("eventId", "userId");
                    table.HasData(
                        new { eventId = 1, userId = 3 },
                        new { eventId = 3, userId = 4 },
                        new { eventId = 3, userId = 2 },
                        new { eventId = 3, userId = 5 }
                        );
                }
                );

            // Events and Groups
            // Events have many groups, groups have many events
            modelBuilder.Entity<Event>()
                .HasMany(e => e.GroupsInvited)
                .WithMany(g => g.EventInvites)
                .UsingEntity<Dictionary<string, object>>(
                "InvitedGroups",
                ig => ig.HasOne<Group>().WithMany().HasForeignKey("groupId"),
                ig => ig.HasOne<Event>().WithMany().HasForeignKey("eventId"),
                table =>
                {
                    table.HasKey("groupId", "eventId");
                    table.HasData(
                        new { groupId = 1, eventId = 2 }
                        );
                }
                );

        }



    }
}

