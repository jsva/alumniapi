﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Models.DTO.Group
{
    public class GroupEditDTO
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool isPrivate { get; set; }
    }
}
