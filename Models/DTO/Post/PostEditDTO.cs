﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Models.DTO.Post
{
    public class PostEditDTO
    {
        public int id { get; set; }
        public string title { get; set; }
        public string text { get; set; }
    }
}
