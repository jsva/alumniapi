﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Models.DTO.Post.Comment
{
    public class CommentEditDTO
    {
        public int id { get; set; }
        public string text { get; set; }
    }
}
