﻿using alumniAPI.Models.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Models.DTO.Post.Comment
{
    public class CommentReadDTO
    {
        public int id { get; set; }
        public MinimalUserReadDTO user { get; set; }
        public DateTime lastUpdated { get; set; }
        public string text { get; set; }
        public IEnumerable<CommentReadDTO> comments { get; set; }

    }
}
