﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Models.DTO.Post.Comment
{
    public class CommentPostDTO
    {
        public int postId { get; set; }
        public string text { get; set; }
        public int parentCommentId { get; set; }
    }
}
