﻿using alumniAPI.Models.DTO.Post.Comment;
using alumniAPI.Models.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Models.DTO.Post
{
    public class PostReadDTO
    {
        public int id { get; set; }
        public MinimalUserReadDTO user { get; set; }
        public DateTime lastUpdated { get; set; }
        public string title { get; set; }
        public string text { get; set; }
        public int? targetGroupId { get; set; }
        public int? targetUSerId { get; set; }
        public int? targetEventId { get; set; }
        public int? targetTopicId { get; set; }

        public  IEnumerable<CommentReadDTO> Comments { get; set; }
    }
}
