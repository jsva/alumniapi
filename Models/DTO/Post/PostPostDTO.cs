﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Models.DTO.Post
{
    public class PostPostDTO
    {
        public string title { get; set; }
        public string text { get; set; }
        public int? targetGroupId { get; set; }
        public int? targetUserId { get; set; }
        public int? targetEventId { get; set; }
        public int? targetTopicId { get; set; }
    }
}
