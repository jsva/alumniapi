﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Models.DTO.Topic
{
    public class TopicReadDTO { 

        public int id { get; set; }

        public string name { get; set; }

        public string description { get; set; }
        public List<int> UsersFollow { get; set; }
        public List<int> EventInvited { get; set; }

    } 
}