﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Models.DTO.Topic
{
    public class TopicPostDTO
    {
        public int userId { get; set; }

        public string name { get; set; }

        public string description { get; set; }


    }
}
