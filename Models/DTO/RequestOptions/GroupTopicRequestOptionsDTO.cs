﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Models.DTO.RequestOptions
{
    public class GroupTopicRequestOptionsDTO
    {
        public string? search { get; set; } = "";

        public string? order { get; set; } = "DESC";

        public int? limit { get; set; } = 0;

        public int? offset { get; set; } = 0;

    }
}
