﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Models.DTO.Event
{
    public class EventEditDTO
    {
        public int id { get; set; }
        public string name { get; set; }
        public int createdBy { get; set; }
        public string description { get; set; }
        public bool allowGuests { get; set; }
        public DateTime startTime { get; set; }
        public DateTime endTime { get; set; }
    }
}
