﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Models.DTO.User
{
    public class MinimalUserReadDTO
    {
        public int id { get; set; }
        public string username { get; set; }
    }
}
