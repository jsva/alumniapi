﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Models.DTO.User
{
    public class UserReadDTO
    {
        public int id { get; set; }
        public string Username { get; set; }
        public string Img { get; set; }
        public string Status { get; set; }
        public string Bio { get; set; }
        public string FunFact { get; set; }

        public List<int> Groups { get; set; }
        public List<int> Topics { get; set; }
        public List<int> Events { get; set; }
    }
}

