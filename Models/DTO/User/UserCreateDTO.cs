﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Models.DTO.User
{
    public class UserCreateDTO
    {
        public string Username { get; set; }
        public string Img { get; set; }
        public string Status { get; set; }
        public string Bio { get; set; }
        public string FunFact { get; set; }
    }
}
