﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Enums
{
    public enum PostTypes
    {
        GROUP,
        EVENT,
        TOPIC,
        USER,
        ALL
    }
}
