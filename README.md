# AlumniAPI

API for alumni 
Azure: https://alumniapi.azurewebsites.net/

## Team:
Amalie Espeseth - https://gitlab.com/amalie.e

Erlend Hollund - https://gitlab.com/Holdude

Jostein Skadberg - https://gitlab.com/josteinskadberg

Jonas Sv�sand - https://gitlab.com/jsva


## Description
This is the backend for a project to make a web-app to host a portal for Alumni of a given entity.

It is written in C# on the .Net platform. The backend is a REST-API.

It is hosted on Azure web apps, and is connected to a SQL database also hosted on azure.
Usage of the API requires authentication that we have solved through hosting our own auth service, using
Keycloak. 


## Instructions

### How to use:
There is no way for externals to get data from this API as it requires autherization and authentication. Only the applications approved by us can get such authentication and autherization and therefore this API is limited to use by our own frontend app.

The only way to view the data from our hosted version is to use the frontend app hosted at:

**https://alumnifront.herokuapp.com/**

See https://gitlab.com/josteinskadberg/alumnifront to host your own version of the frontend app.

### How to host your own version
You can deploy your own version and implement your own authentication and autherization if that is desired.

**Step by step guide for own deployment:**
1. Clone the repository to your computer
2. Open solution in Visual Sudio or other editor/IDE.
3. Replace the auth service links with your own in startup.cs
   - You need to provide one link for where to get the keys and one for a valid base url. in startup.cs
4. Replace the SQL database link with your own.
   - You can have the url hosted in you appsettings.json. If you use the same keynames that you find in startup.cs you will not need to do any changes in startup.cs
   - In startup.cs there is a setup for having one local database and one hosted in azure, this is not nescessary you need only one db, but it should be hosted online and available on the net
   - If you make a SQL server and database in azure they will provide you with the connection string neeeded. 
5. Migrate the database structure to your database
   - This can be done in the nuget package manager console with the command `update-database`
   - If you want you can delete the current migrations and remove the seeded data from AlumniDbContext.cs. You will then need to re-migrate before updating the database with the comand `add-migration`
6. In UserController one of the GET methods uses redirection based on an url, replace this with an appropiate link according to your host.
7. Upload the code to a hosting service, e.g. Azure Web Apps. This can be done directly through visual studio, by right clicking your solution and selecting "publish".
8. You should now be able to do requests to the hosted backend provided you have the appropiate authentication headers, from *YOUR* authservice in the requests.

## Endpoints
See APIDocumentation.md for a full list and explanation of endpoints. Alternatively, 
you can run the API locally to see swagger documentation of the API.

### Optional headers
Some of the GET methods allow an optional header for the purpose of searching, filtering, reordering, and paginating the results.
These are also explained in the APIDocumentation but are not found in the swagger documentation.
Here is a short explanation of the optional `requestoptions` header you can use to search, order and paginate the responses from the API.


On the GET /post and GET /event you can use the following JSON as the header with name "requestoptions" :
```json
{
    "search": "",
    "groups": [0],
    "topics": [0],
    "order": "",
    "limit": 0,
    "offset": 0
} 
```


For the other GETs for post, and GET /group and GET /topic use the following JSON with name "requestoptions": 
```json 
{
    "search": "",
    "order": "",
    "limit": 0,
    "offset": 0
}
```

If you leave it as it is here, it will be ignored and no restrictions will be placed.

Note that you can not have an offset without a limit.
Accepted order arguments are as follow:
- Post : `ASC/DESC + - + [,LASTUPDATED, TITLE, GROUP, TOPIC, USER, USERNAME] E.G "ASC-GROUP"`
- Event :`ASC/DESC + - + [,LASTUPDATED,NAME,START,END,CREATOR,DESCRIPTION] E.G "ASC-START"`
- Group: `ASC/DESC + - + [NAME, DESCRIPTION, PRIVATE, ID, MEMBERS]  E.G "ASC-NAME"`
- Topic : `ASC/DESC + - + [NAME, DESCRIPTION, ID, MEMBERS]  E.G "ASC-MEMBERS"`