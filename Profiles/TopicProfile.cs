﻿using alumniAPI.Models.Domain;
using alumniAPI.Models.DTO.Topic;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Profiles
{
    public class TopicProfile : Profile
    {
        public TopicProfile()
        {
            CreateMap<TopicPostDTO, Topic>();
            CreateMap<Topic, TopicReadDTO>()
                .ForMember(tdto => tdto.UsersFollow, opt => opt
                .MapFrom(t => t.UsersFollow.Select(u => u.id).ToArray()))
                .ForMember(tdto => tdto.EventInvited, opt => opt
                .MapFrom(t => t.EventInvited.Select(e => e.id).ToArray()))
                ;
        }
    }
}
