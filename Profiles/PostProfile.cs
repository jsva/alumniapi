﻿using alumniAPI.Models;
using alumniAPI.Models.Domain;
using alumniAPI.Models.DTO.Post;
using alumniAPI.Models.DTO.User;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Profiles
{
    public class PostProfile : Profile
    {
        public PostProfile()
        {
            CreateMap<Post, PostReadDTO>()
                .ForMember(pdto => pdto.user, opt => opt
                .MapFrom(p => new MinimalUserReadDTO()
                {
                    username = p.user.username,
                    id = p.user.id
                }));
            CreateMap<PostReadDTO, Post>();
            CreateMap<PostEditDTO, Post>();
            CreateMap<PostPostDTO, Post>();
        }
    }
}
