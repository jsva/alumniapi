﻿using alumniAPI.Models.Domain;
using alumniAPI.Models.DTO.Event;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Profiles
{
    public class EventProfile : Profile
    {
       public EventProfile()
        {
            CreateMap<Event, EventReadDTO>()
                .ForMember(edto => edto.RSVP, opt => opt
                .MapFrom(e => e.RSVPList.Select(e => e.id).ToArray()))
                .ForMember(edto => edto.Group, opt => opt
                .MapFrom(e => e.GroupsInvited.Select(e => e.id).ToArray()))
                .ForMember(edto => edto.Users, opt => opt
                .MapFrom(e => e.UserInvited.Select(e => e.id).ToArray()))
                .ForMember(edto => edto.Topics, opt => opt
                .MapFrom(e => e.TopicInvited.Select(e => e.id).ToArray()))
                .ReverseMap();
            CreateMap<EventEditDTO, Event>();
            CreateMap<EventCreateDTO, Event>();

        }

    }
}
