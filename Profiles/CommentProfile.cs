﻿using alumniAPI.Models.Domain;
using alumniAPI.Models.DTO.Post.Comment;
using alumniAPI.Models.DTO.User;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Profiles
{
    public class CommentProfile : Profile
    {
        public CommentProfile()
        {
            CreateMap<Comment, CommentReadDTO>().ForMember(cdto => cdto.user, opt => opt
            .MapFrom(c => new MinimalUserReadDTO()
            {
                username = c.user.username,
                id = c.user.id
            })).ReverseMap();
            CreateMap<CommentEditDTO, Comment>();
            CreateMap<CommentPostDTO, Comment>();
        }
    }
}
