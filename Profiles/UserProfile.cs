﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumniAPI.Models;
using alumniAPI.Models.DTO.User;

namespace alumniAPI.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserReadDTO>()
                .ForMember(udto => udto.Groups, opt => opt
                .MapFrom(u => u.Groups.Select(u => u.id).ToArray()))
                .ForMember(udto => udto.Events, opt => opt
                .MapFrom(u => u.Events.Select(u => u.id).ToArray()))
                .ForMember(udto => udto.Topics, opt => opt
                .MapFrom(u => u.Topics.Select(u => u.id).ToArray()))
                .ReverseMap();
            CreateMap<UserEditDTO, User>();
            CreateMap<UserCreateDTO, User>();
        }
    }
}
