﻿using alumniAPI.Models;
using alumniAPI.Models.DTO.Group;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Profiles
{
    public class GroupProfile : Profile
    {
        public GroupProfile()
        {
            CreateMap<Group, GroupReadDTO>()
                .ForMember(gdto => gdto.Users, opt => opt
                .MapFrom(g => g.GroupMembers.Select(u => u.id).ToArray()))
                .ForMember(gdto => gdto.Events, opt => opt
                .MapFrom(g => g.EventInvites.Select(e => e.id).ToArray()))
                .ReverseMap();

            CreateMap<GroupEditDTO, Group>();
            CreateMap<GroupCreateDTO, Group>();
        }
    }
}
