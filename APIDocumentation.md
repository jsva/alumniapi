﻿# API Documentation
 *For Alumni API*

This is a REST api written in .NET core 5

# Headers :
Here the headers that are specific to this project are described:

### Authorization header:
All endpoints are protected from unauthorized access and you need to provide an authentication
token in order to access the endpoint. 

The **key** for this header is `Authorization`

The **value** for the header should be a string consisting of `Bearer ` followed  by the token .

### Request options header
For some of the GET endpoints there is an optional header that can be used to alter the data you will get back.
This in the form of a search, filter, reordering or pagination of the results, all of these can also be comined.
The endpoints that accepts such a header will be specified in the section below.
They **key** for this header is `requestoptions`.
There are two different JSON objects which can be used for these request options, depending
on if the endpoint accepts filtering of results or not.

They are as follow:

Without filter : 

```json
{
"search": "",
  "order": "",
  "limit": 0,
  "offset": 0 
}
```

With filter : 
```json
{
  "search": "",
  "groups": [
    0
  ],
  "topics": [
    0
  ],
  "order": "",
  "limit": 0,
  "offset": 0
} 
```

In this document these two will be referred to as requestoptions and requestoptions with filter.

The different attributes require different data:
- Search takes a string of a search term, it can be a word, a partial word, a letter or several words together. The searchterm is not case sensitive, and an empty string, will ignore the search.
- groups and topics accepts a list of ids for the groups and topics you wish to have included as targets for events/posts that you request. If you return an empty list, or a list with just one element of 0, no filtering will be applied.
- order is a string that describes what order you want(ascending vs. descending) and what attribute to order on. All endpoints which can take such a requestoption will have specified which attributes it can be sorted on. If no valid order is given, it will return a default ordering.
- limit describes an upper limit on how many elements you want returned. If set to 0 it will be ignored and no limit will be applied.
- offset describes how many elements you wish to ignore. together with limit, this can be used to achieve pagination. **Note:** You can not use offset if you have no limit. If set to 0 it will be ignored and no offset will be applied.


# How to use:  

Below only the last part of the path is given. For the API deployed by the authors
you will need to prepend `https://alumniapi.azurewebsites.net/` to the paths. If
you host your own API you will need to prepend your own host to the endpoints.

Parts of url in brackets ( { example /} ) are variable that should be replaced with appropriate values

For data that is to be posted, replace the value of the json with appropiate data, unless stated otherwise.

**ALL ENDPOINTS REQUIRE THE AUTHORIZATION HEADER**



## Events
### Endpoints
- **GET /api/Event**
  - Gets all events from the database
  - Can take a requestoptions header with filter
    - Order options: `ASC/DESC + - + [,LASTUPDATED,NAME,START,END,CREATOR,DESCRIPTION] E.G "ASC-START"`
  - Does not accept a body
  - All events that user is not invited to or not creator of is automatically filtered out
  - HTTP responses: 
    - 200
  - No errors possible, but if you have no invitations to events you will get an empty list returned
- **POST /api/Event**
  -  Creates a new event in the database
  -  Takes a new event json in body 
  -  Will return the newly created event if successfull
  -  HTTP responses:
     -  201
     -  500
        -  If something goes wrong with the process internally
- **GET /api/Event/{id\}**  
  - Gets one event with a specified id
  - specify the id in the endpoint URL
  - Does not take any additional information in body or header
  - Returns one event
  - HTTP responses:
    - 200
    - 404
      - If there is no event with the id specified
- **PUT /api/Event/{eventId\}**  
  - Edits the event in the database with the specified id
  - Provide the edited event in the body, and the id of the event in the url
  - HTTP responses:
    - 204
    - 400
      - If the body is not properly formatted, e.g. id does not match id in url
    - 403
      - If you are not the creator you are not allowed to edit the event
    - 404
      - If there is no event with the specified id
- **POST  /api/Event/{eventId\}/invite/group/{groupId\}** 
  - Invite a group to an event, only event creator may invite. 
  - Creates an invitation relationship between the group and event in the database
  - Takes no body or additional headers. specify eventid and groupid in url
  - HTTP responses:
    - 204
    - 403
      - Only event creator is allowed to invite
    - 404
      - If the specified event OR group does not exist
- **DELETE /api/Event/{eventId\}/invite/group/{groupId\}**  
  - Delete a group invite, only even creator may delete an invitation
  - - Deletes a invitation relationship between a user and event
  - Takes no additional headers or body, specify eventid and groupid in url
  - HTTP responses:
    - 204
    - 403
      - Only event creator may delete an invitation
    - 404
      - If the event or user does not exist.
- **POST /api/Event/{eventId\}/invite/user/{userId\}**  
  - Invites a single user, only event creator may invite a user
  - Creates an invitation relationship between the user and event in the database
  - Takes no body or additional headers. Specify eventid and groupid in url
  - HTTP responses:
    - 204
    - 403
      - Only event creator is allowed to invite
    - 404
      - If the specified event OR user does not exist
- **DELETE  /api/Event/{eventId\}/invite/user/{userId\}**  
  - Deletes a invite to a single user, only the event creator may delete an invitation
  - Deletes a invitation relationship between a user and event
  - Takes no additional headers or body, specify eventid and userid in url
  - HTTP responses:
    - 204
    - 403
      - Only event creator may delete an invitation
    - 404
      - If the event or user does not exist.
- **POST /api/Event/{eventId\}/invite/topic/{topicId\}** 
  - Invites a topic to an event, only allowed if you are event creator
  - Creates an invitation relationship between the topic and event in the database
  - Takes no body or additional headers. Specify eventid and topicid in url
  - HTTP responses:
    - 204
    - 403
      - Only event creator is allowed to invite
    - 404
      - If the specified event OR topic does not exist
- **DELETE /api/Event/{eventId\}/invite/topic/{topicId\}**  
  - Removes an invite to a topic from an event, only the event creator may delete an invitation
  - Deletes a invitation relationship between a topic and event
  - Takes no additional headers or body, specify eventid and topicid in url
  - HTTP responses:
    - 204
    - 403
      - Only event creator may delete an invitation
    - 404
      - If the event or topic does not exist.

 ### Data structures:

All GET method returns this JSON or a list of this JSON: 

```json {
        "id": 0,
        "name": "string",
        "createdBy": 0,
        "lastUpdated": "2022-03-21T12:08:26.179Z",
        "description": "string",
        "allowGuests": true,
        "startTime": "2022-03-21T12:08:26.179Z",
        "endTime": "2022-03-21T12:08:26.179Z",
        "rsvp": [0],
        "users": [0],
        "topics": [0],
        "group": [0]
      }  
```

When using the Post event use the first of the following JSON, use the 2nd for the PUT event endpoint. Times are in UTC.

```json   
{
"name": "string",
"createdBy": 0,
"description": "string",
"allowGuests": true,
"startTime": "2022-03-21T12:07:43.078Z",
"endTime": "2022-03-21T12:07:43.078Z"
}
```
```json
{
"id": 0,
"name": "string",
"createdBy": 0,
"description": "string",
"allowGuests": true,
"startTime": "2022-03-21T12:09:23.350Z",
"endTime": "2022-03-21T12:09:23.350Z"
} 
```

## Group
### Endpoints

- **GET /api/Group** 
  - Gets all groups from the database
  - Can take a requestoptions header without filter
    - Order options: `ASC/DESC + - + [NAME, DESCRIPTION, PRIVATE, ID, MEMBERS]  E.G "ASC-NAME"`
  - Does not accept a body
  - Private groups that the user is not a member of, is filtered out of the results automatically
  - HTTP responses: 
    - 200
- **POST /api/Group** 
  - Creates a new group and adds the creator as its first member
  - Creates a group in the database, and also a groupmembership in the database between the user and new group
  - Takes a new group JSON in body, no additional headers
  - HTTP responses:
    - 201
- **GET /api/Group/{id\}** 
  - Gets a group  with the specified id
  - Returns the group with the specified id if it exists
  - Takes no body or additional headers
  - HTTP responses:
    - 200
    - 403
      - If the group is private and requesting user is not member, they will not be permitted to get this group
    - 404
      - If there is no group with the specified id
- **POST /api/Group/{groupId\}/Join/userId** 
  - Add a new user to group
  - Can either have the requesting user join the group themselves, or add another user to a group they are a member of.
  - One can not join private groups, only be added by existing members
  - replace groupId in url with the groups id. 
  - If you wish to add another user to group, add `?userid={id}` with id being that users id, to the end of the url
  - HTTP responses:
    - 204
    - 403
      - If you are trying to join a private group, or adding another user to a group you are not member of.
    - 404
      - if no group exists with the provided id
 ### Data structures:

All GET method returns this JSON or a list of this JSON: 
```json
{
"id": 0,
"name": "string",
"description": "string",
"isPrivate": true,
"users": [0],
"events": [0]
}
```
For POST-ing a new group to the API use the following JSON in the body:
```json
{
"name": "string",
"description": "string",
"isPrivate": true
}
```

## Post
### Endpoints

In those endpoints that can use requestoptions the following orders are allowed:

`ASC/DESC + - + [,LASTUPDATED, TITLE, GROUP, TOPIC, USER, USERNAME] E.G "ASC-GROUP"`

- **GET /api/Post**  
  - Get all posts (and their comments) that the user has access through their membership in groups and topics
  - Does not return posts that are targeted at events or users
  - Accepts a requestoption with filter in the header, does not take a body
  - All posts that are returned includes the comments on the post, and the comments child comments etc.
  - HTTP responses:
    - 200
- **POST /api/Post**  
  - Add a new Post to the database
  - Takes a body consisting of a new post JSON object.
  - Takes no additional headers
  - You need to be a member of the group/topic you are targetting the post towards, or invited if its a event. Post to users are always allowed.
  - The newly created post will be returned (including its id)
  - HTTP responses:
    - 201
    - 403
      - If you are trying to post to an audience which you are not allowed, e.g. a event you are not invited to 
- **PUT /api/Post/{id\}**  
  - Edits the values of a post(text or title) 
  - The lastUpdated will be changed autocmatically by the API
  - Accepts a body containing the new text and title in JSON format, takes no additional headers
  - Takes an id of the post that is to be changed, in the url
  - HTTP responses:
    - 204
    - 400
      - If the JSON is not formatted correctly or the id does not match
    - 403
      - Only the original creator of a post is allowed to change its values
    - 404
      - Did not find a post with the specified id
- **GET /api/Post/user**  
  - Gets all the posts with the querying user as target audience
  - Essentially an endpoint to get all direct messages to the user
  - Accepts a requestoptions header without filter, takes no body
  - Takes no id in url as API knows the id of the querying user
  - HTTP responses:
    - 200
  - If user has received no messages it will still give 200, but with an empty list as response
- **GET /api/Post/user/{id\}**  
  - Returns all posts the querying user has received from another specified user
  - An endpoint that lets you find all posts another user has targeted to the current user
  - replace the id in url with the other users userid
  - Takes an optional requestoptions header without filter, no body
  - HTTP responses:
    - 200
  - If no posts between the two users, it will return an empty list
- **GET /api/Post/group/{id\}**  
  - Gets all posts to a specified group
  - specify the groups id in the url
  - Takes an optional requestoptions headers without filter, no body
  - HTTP responses: 
    - 200
    - 403
      - If you are not member of the group you will not be allowed to request the posts in the group
    - 404
      - If there is no group with the specified id
- **GET /api/Post/event/{id\}**  
  - Gets all posts to a specified event
  - specify the events id in the url
  - Takes an optional requestoptions headers without filter, no body
  - HTTP responses: 
    - 200
    - 403
      - If you are not invited to the event you will not be allowed to request the posts belonging to the event
    - 404
      - If there is no event with the specified id
- **GET /api/Post/topic/{id\}**  
  - Gets all posts to a specified topic
  - specify the groups id in the url
  - Takes an optional requestoptions headers without filter, no body
  - HTTP responses: 
    - 200
    - 403
      - If you are not member of the topic you will not be allowed to request the posts in the topic
    - 404
      - If there is no topic with the specified id
- **POST /api/Post/Comment**  
  - Post a new comment to the API
  - Takes the content of the new comment in JSON form in the body
  - no additional headers 
  - A comment requires a post to be linked to
  - You also need to be a member of the group/topic the post is posted in, or invited to the event it is posted to.
  - HTTP responses:
    - 201
    - 403
      - If the comment tries to reply to a post the user is not allowed to see (e.g. in a group they are not member of)
- **PUT /api/Post/Comment/{id\}**  
  - Edit a comment in the API
  - Takes the edited comment in body, and the id of the comment to edit in url. No additional headers
  - HTTP responses:
    - 204
    - 400
      - If the commentid provided in the body does not match the id in the url
    - 403
      - If a user tries to edit a comment they did not create themselves
    - 404
      - If no comment with the given id exists
### Data structures:
All the GET method returns a list of the following JSON object. Note that the comment attribute
can nest itself, and e.g. be a list of other comments.
```json
{
"id": 0,
"user":{
        "username": "string",
         "id" : 0
        },
"lastUpdated": "2022-03-21T12:11:34.671Z",
"title": "string",
"text": "string",
"targetGroupId": 0,
"targetUSerId": 0,
"targetEventId": 0,
"targetTopicId": 0,
"comments": [{
    "id": 0,
    "user":{
        "username": "string",
         "id" : 0
        },
    "lastUpdated": "2022-03-21T12:11:34.671Z",
    "text": "string",
    "comments": [null]
    }]
}
```

For POST-ing a new post to the API use this JSON object. Set the target of the post to the id
of the group/user/event/topic you are targetting. Set the others to 0.

```json 
{
"title": "string",
"text": "string",
"targetGroupId": 0,
"targetUserId": 0,
"targetEventId": 0,
"targetTopicId": 0
}
```

For changing a post with PUT, use this JSON object:
```json
{
"id": 0,
"title": "string",
"text": "string"
}
```
To POST a comment to the API use the following JSON. If it is a top level comment, then set 
parentCommentId to 0. postId corrsponds to the post this comment is replying to.

```json
{
"postId": 0,
"text": "string",
"parentCommentId": 0
} 
```
To edit a comment with PUT use this JSON:
```json
{
"id": 0,
"text": "string"
}
```

## Topic

### Endpoints
- **GET /api/Topic**  
  - Get all the topics
  - Accepts an optional requestoptions header without filter. Takes No Body
    - Order options `ASC/DESC + - + [NAME, DESCRIPTION, ID, MEMBERS]  E.G "ASC-MEMBERS"`
  - HTTP responses:
    - 200
- **POST /api/Topic**  
  - Add a new topic to the API
  - Takes a body of a new Topic JSON object, takes no additional headers
  - returns the newly created topic
  - HTTP responses:
    - 201
- **GET /api/Topic/{id\}**  
  - Get a specific topic by id
  - the id is specified in the url
  - Takes no body or additional headers
  - HTTP responses:
    - 200
    - 404
      - If there is no topic with the specified id
- **POST  /api/Topic/{topicId\}/join** 
  - Adds the requesting user to the membership of the topic corresponding to the provided id
  - Only need to provide the topics id in url, the API will know the users id
  - Takes no body or additional headers
  - HTTP responses:
    - 204
    - 404
      - If there is no topic with the specified id

### Data structures:
Response from API when you request topics, you get one or a list of the following JSON:
```json
{
"id": 0,
"name": "string",
"description": "string",
"usersFollow": [0],
"eventInvited": [0]
}
```
When POST-ing a new topic to the api use the following JSON object:
```json
{
"userId": 0,
"name": "string",
"description": "string"
}
```



## User
### Endpoints
- **GET /api/User**  
  - This endpoint finds the userid of the requesting user and then redirects to the GET /api/user/id, with the id of the requesting user
  - This allows a request to get the user that is currently logged in without knowing their id, as long as they have a valid authorization token
  - Takes no additional headers and no body
  - If the requesting user is not registered in the api, they will be and get redirected with their newly created id
  - HTTP responses:
    - 307
- **GET /api/User/{id\}**  
  - Gets a specific user by id
  - takes no body or additional headers
  - HTTP responses:
    - 200
    - 404
      - If no user with the id exists
- **PUT /api/User/{id\}**  
  - Edits an user
  - Takes a body consisting of a edit user JSON object
  - Takes the id of the user to be edited in the url
  - Takes no additional headers
  - HTTP responses:
    - 204
    - 400
      - Userid in body does not match user id in url
    - 403
      - User trying to edit another users user object. You can only edit your own profile
    - 404
      - No user with the specified id found

### Data structures:

The following JSON object is what is returned when requesting a user from the api
```json
{
"id": 0,
"username": "string",
"img": "string",
"status": "string",
"bio": "string",
"funFact": "string",
"groups": [0],
"topics": [0],
"events": [0]
}
```
When editing a user with PUT use this JSON object. the id should not be changed, and is only there to verfiy that you are changing the user you intend to change.:
```json
{
"id": 0,
"img": "string",
"status": "string",
"bio": "string",
"funFact": "string"
}
```