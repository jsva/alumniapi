using alumniAPI.Models;
using alumniAPI.Services;
using alumniAPI.Services.EventService;
using alumniAPI.Services.GroupService;
using alumniAPI.Services.PostService;
using alumniAPI.Services.RequestOptionService;
using alumniAPI.Services.TokenService;
using alumniAPI.Services.TopicService;
using alumniAPI.Services.UserServices;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;

namespace alumniAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddAutoMapper(typeof(Startup));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Alumni", Version = "v1" });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

            });
            services.AddScoped(typeof(IUserService), typeof(UserService));
            services.AddScoped(typeof(IGroupService), typeof(GroupService));
            services.AddScoped(typeof(IPostService), typeof(PostService));
            services.AddScoped(typeof(IEventService), typeof(EventService));
            services.AddScoped(typeof(ITokenService), typeof(TokenService));
            services.AddScoped(typeof(ITopicService), typeof(TopicService));
            services.AddScoped(typeof(IRequestOptionService), typeof(RequestOptionService));


            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                  .AddJwtBearer(options =>
                  {
                      options.TokenValidationParameters = new TokenValidationParameters
                      {
                          //Access token for postman can be found at http://localhost:8000/#
                          //requires token from keycloak instance - location stored in secret manager
                          IssuerSigningKeyResolver = (token, securityToken, kid, parameters) =>
                          {
                              var client = new HttpClient();
                              var keyuri = "https://alumni-auth.herokuapp.com/auth/realms/Alumni/protocol/openid-connect/certs";
                              //Retrieves the keys from keycloak instance to verify token
                              var response = client.GetAsync(keyuri).Result;
                              var responseString = response.Content.ReadAsStringAsync().Result;
                              var keys = JsonConvert.DeserializeObject<JsonWebKeySet>(responseString);
                              return keys.Keys;
                          },

                          ValidIssuers = new List<string>
                      {
                        "https://alumni-auth.herokuapp.com/auth/realms/Alumni"
                      },

                          //This checks the token for a the 'aud' claim value
                          ValidAudience = "account",
                      };
                  });


            services.AddControllers();
            services.AddDbContext<AlumniDbContext>(opt =>
            //opt.UseSqlServer(Configuration.GetConnectionString("AzureConnection")));
            opt.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "alumniAPI v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
