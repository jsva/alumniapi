﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace alumniAPI.Migrations
{
    public partial class InitalizeDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Event",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    lastUpdated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    createdBy = table.Column<int>(type: "int", nullable: false),
                    description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    allowGuests = table.Column<bool>(type: "bit", nullable: false),
                    bannerImg = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    startTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    endTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Event", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Group",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    isPrivate = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Group", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Topic",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Topic", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    username = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: false),
                    img = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    status = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    bio = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    funFact = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "InvitedGroups",
                columns: table => new
                {
                    groupId = table.Column<int>(type: "int", nullable: false),
                    eventId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvitedGroups", x => new { x.groupId, x.eventId });
                    table.ForeignKey(
                        name: "FK_InvitedGroups_Event_eventId",
                        column: x => x.eventId,
                        principalTable: "Event",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InvitedGroups_Group_groupId",
                        column: x => x.groupId,
                        principalTable: "Group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EventTopic",
                columns: table => new
                {
                    EventInvitedid = table.Column<int>(type: "int", nullable: false),
                    TopicInvitedid = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventTopic", x => new { x.EventInvitedid, x.TopicInvitedid });
                    table.ForeignKey(
                        name: "FK_EventTopic_Event_EventInvitedid",
                        column: x => x.EventInvitedid,
                        principalTable: "Event",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EventTopic_Topic_TopicInvitedid",
                        column: x => x.TopicInvitedid,
                        principalTable: "Topic",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GroupMembers",
                columns: table => new
                {
                    groupId = table.Column<int>(type: "int", nullable: false),
                    userId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupMembers", x => new { x.groupId, x.userId });
                    table.ForeignKey(
                        name: "FK_GroupMembers_Group_groupId",
                        column: x => x.groupId,
                        principalTable: "Group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GroupMembers_User_userId",
                        column: x => x.userId,
                        principalTable: "User",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InvitedUsers",
                columns: table => new
                {
                    eventId = table.Column<int>(type: "int", nullable: false),
                    userId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvitedUsers", x => new { x.eventId, x.userId });
                    table.ForeignKey(
                        name: "FK_InvitedUsers_Event_eventId",
                        column: x => x.eventId,
                        principalTable: "Event",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InvitedUsers_User_userId",
                        column: x => x.userId,
                        principalTable: "User",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Post",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    lastUpdated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    text = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    userId = table.Column<int>(type: "int", nullable: false),
                    title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    targetGroupId = table.Column<int>(type: "int", nullable: false),
                    targetUserId = table.Column<int>(type: "int", nullable: false),
                    targetEventId = table.Column<int>(type: "int", nullable: false),
                    targetTopicId = table.Column<int>(type: "int", nullable: false),
                    Groupid = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Post", x => x.id);
                    table.ForeignKey(
                        name: "FK_Post_Group_Groupid",
                        column: x => x.Groupid,
                        principalTable: "Group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Post_User_userId",
                        column: x => x.userId,
                        principalTable: "User",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "RSVP",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserFK = table.Column<int>(type: "int", nullable: true),
                    EventFK = table.Column<int>(type: "int", nullable: true),
                    lastUpdated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    guestCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RSVP", x => x.id);
                    table.ForeignKey(
                        name: "FK_RSVP_Event_EventFK",
                        column: x => x.EventFK,
                        principalTable: "Event",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RSVP_User_UserFK",
                        column: x => x.UserFK,
                        principalTable: "User",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TopicUser",
                columns: table => new
                {
                    Topicsid = table.Column<int>(type: "int", nullable: false),
                    UsersFollowid = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TopicUser", x => new { x.Topicsid, x.UsersFollowid });
                    table.ForeignKey(
                        name: "FK_TopicUser_Topic_Topicsid",
                        column: x => x.Topicsid,
                        principalTable: "Topic",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TopicUser_User_UsersFollowid",
                        column: x => x.UsersFollowid,
                        principalTable: "User",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comment",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    text = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    lastUpdated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    parentCommentId = table.Column<int>(type: "int", nullable: false),
                    commentid = table.Column<int>(type: "int", nullable: true),
                    postId = table.Column<int>(type: "int", nullable: false),
                    userId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comment", x => x.id);
                    table.ForeignKey(
                        name: "FK_Comment_Comment_commentid",
                        column: x => x.commentid,
                        principalTable: "Comment",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Comment_Post_postId",
                        column: x => x.postId,
                        principalTable: "Post",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_Comment_User_userId",
                        column: x => x.userId,
                        principalTable: "User",
                        principalColumn: "id");
                });

            migrationBuilder.InsertData(
                table: "Event",
                columns: new[] { "id", "allowGuests", "bannerImg", "createdBy", "description", "endTime", "lastUpdated", "name", "startTime" },
                values: new object[,]
                {
                    { 1, true, "https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Flag_of_Ukraine.svg/1200px-Flag_of_Ukraine.svg.png", 3, "Get you AK's and MIG's ready, we are going on our yearly vacation trip to Kyiv!", new DateTime(2030, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 3, 15, 11, 34, 56, 755, DateTimeKind.Local).AddTicks(3538), "The grand I̶n̶v̶a̶s̶i̶o̶n̶  vacaction trip to Ukraine", new DateTime(2022, 2, 24, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, true, "https://2yrh403fk8vd1hz9ro2n46dd-wpengine.netdna-ssl.com/wp-content/uploads/2017/06/2017-Yamaha-YZF-R3-Review-Motorcycle-14.jpg", 2, "Prepare for the biggest spring rideout in Norway!", new DateTime(2022, 4, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 3, 15, 11, 34, 56, 794, DateTimeKind.Local).AddTicks(7870), "Rideout MC Trip", new DateTime(2022, 4, 10, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, true, "https://www.business2community.com/wp-content/uploads/2014/10/stealing-content.png.png", 4, "Got someting valueable? Let me take a look and give you an offer!", new DateTime(2022, 5, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 3, 15, 11, 34, 56, 794, DateTimeKind.Local).AddTicks(8115), "Bring all your valuables", new DateTime(2022, 5, 20, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "Group",
                columns: new[] { "id", "description", "isPrivate", "name" },
                values: new object[,]
                {
                    { 1, "Group for motorcycle enthusiasts. All things MC", false, "Motorcycle" },
                    { 2, "The nr 1 place to go for football news and discussions", false, "Football" },
                    { 3, "For those who aim high", false, "Climbing" },
                    { 4, "Ronny's closest friends", true, "Ronny and Friends" }
                });

            migrationBuilder.InsertData(
                table: "Topic",
                columns: new[] { "id", "description", "name" },
                values: new object[,]
                {
                    { 1, "All things russian/CSGO", "Russian" },
                    { 2, "Imagine an bike, but with an engine instead of your legs", "Motorcycle" }
                });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "id", "bio", "funFact", "img", "status", "username" },
                values: new object[,]
                {
                    { 1, "kedetgåri", "ja", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJODdqu99Oiu-uDdtc2_bF35X7pczodMocCA&usqp=CAU", "Living life", "Ronny" },
                    { 2, "Life is like a MC trip. Fun, speed and death around every corner", "(-■_■)", "https://img.freepik.com/free-photo/cool-man-biker-sunglasses-sitting-near-his-motocycle_93675-86796.jpg?size=626&ext=jpg", "Riding along", "Patrick" },
                    { 3, "Just your average russian life enjoyer", "My Kalashnikov is not the largest thing i own ;)", "https://i.imgflip.com/2mmfa5.jpg", "Currently vacationing in Ukraine <3", "Ivan" },
                    { 4, "Låner ting på permanet basis", "Alt ditt er mitt", "idk", "holder på med ting", "S.V Indel" },
                    { 5, "Jeg vet bedre enn deg", "Jeg er medlem i mensa", "https://christineotterstad.files.wordpress.com/2012/09/besserwissere.jpg", "Opplyser befolkningen", "Bernt Esserwisser" }
                });

            migrationBuilder.InsertData(
                table: "GroupMembers",
                columns: new[] { "groupId", "userId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 3, 1 },
                    { 1, 2 },
                    { 3, 5 },
                    { 2, 5 },
                    { 1, 3 },
                    { 4, 5 },
                    { 1, 5 },
                    { 1, 4 }
                });

            migrationBuilder.InsertData(
                table: "InvitedGroups",
                columns: new[] { "eventId", "groupId" },
                values: new object[] { 2, 1 });

            migrationBuilder.InsertData(
                table: "InvitedUsers",
                columns: new[] { "eventId", "userId" },
                values: new object[,]
                {
                    { 1, 3 },
                    { 3, 2 },
                    { 3, 4 },
                    { 3, 5 }
                });

            migrationBuilder.InsertData(
                table: "Post",
                columns: new[] { "id", "Groupid", "lastUpdated", "targetEventId", "targetGroupId", "targetTopicId", "targetUserId", "text", "title", "userId" },
                values: new object[,]
                {
                    { 2, null, new DateTime(2022, 3, 15, 11, 34, 56, 795, DateTimeKind.Local).AddTicks(7370), 0, 1, 0, 0, "Har noe skikkelig god og billig bensin til salgs. Kan hentes hos meg, betaling på forhånd. 15kr/l. Mvh Svein Vegard Indel ", "Har billig bensin til salgs!", 4 },
                    { 3, null, new DateTime(2022, 3, 15, 11, 34, 56, 795, DateTimeKind.Local).AddTicks(7473), 1, 0, 0, 0, "I am really happy to get to go on a vacation, i hope we will get welcomed as warmly as we have been promised!", "Looking forward to this!!", 3 },
                    { 1, null, new DateTime(2022, 3, 15, 11, 34, 56, 795, DateTimeKind.Local).AddTicks(4023), 0, 1, 0, 0, "Har solgt hjelmen min for å kunne kjøpe litt mer bensin, så lurte på hvor mange andre som kjørte rundt uten hjelm?", "Bruker dokkar hjelm bois?", 2 }
                });

            migrationBuilder.InsertData(
                table: "Comment",
                columns: new[] { "id", "commentid", "lastUpdated", "parentCommentId", "postId", "text", "userId" },
                values: new object[,]
                {
                    { 4, null, new DateTime(2022, 3, 15, 11, 34, 56, 796, DateTimeKind.Local).AddTicks(3558), 0, 1, "In russia we normally don't use helm, but when we now vacation we use special helm from 1944, is very good", 3 },
                    { 5, null, new DateTime(2022, 3, 15, 11, 34, 56, 796, DateTimeKind.Local).AddTicks(3592), 0, 1, "Hvis du trenger ny hjelm for en billig penge så kan jeg selge deg en særdeles god hjelm for en billig penge", 4 },
                    { 6, null, new DateTime(2022, 3, 15, 11, 34, 56, 796, DateTimeKind.Local).AddTicks(3639), 0, 1, "All forskning viser at å bruke hjelm redder liv, å ikke bruke hjelm er dermed et ekstremt dårlig valg.", 5 },
                    { 7, null, new DateTime(2022, 3, 15, 11, 34, 56, 796, DateTimeKind.Local).AddTicks(3670), 5, 1, "Kjøper ikke hjelm fra deg før jeg får den jævla bensinen min!", 1 },
                    { 1, null, new DateTime(2022, 3, 15, 11, 34, 56, 796, DateTimeKind.Local).AddTicks(518), 0, 2, "Hei, jeg kjøper 15 000 liter sånn at jeg får kjørt litt hehehe", 2 },
                    { 2, null, new DateTime(2022, 3, 15, 11, 34, 56, 796, DateTimeKind.Local).AddTicks(2759), 1, 2, "Hvor blir denne bensinen av??? Jeg har jo sendt penger men ikke fått bensin!!!!", 2 },
                    { 3, null, new DateTime(2022, 3, 15, 11, 34, 56, 796, DateTimeKind.Local).AddTicks(3512), 2, 2, "Hvis du hadde lest litt nøyere hadde du kanskje innsett at fyren er bokstavelig talt en S V indler, og burde dermed ikke stoles på.", 5 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comment_commentid",
                table: "Comment",
                column: "commentid");

            migrationBuilder.CreateIndex(
                name: "IX_Comment_postId",
                table: "Comment",
                column: "postId");

            migrationBuilder.CreateIndex(
                name: "IX_Comment_userId",
                table: "Comment",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_EventTopic_TopicInvitedid",
                table: "EventTopic",
                column: "TopicInvitedid");

            migrationBuilder.CreateIndex(
                name: "IX_GroupMembers_userId",
                table: "GroupMembers",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_InvitedGroups_eventId",
                table: "InvitedGroups",
                column: "eventId");

            migrationBuilder.CreateIndex(
                name: "IX_InvitedUsers_userId",
                table: "InvitedUsers",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_Post_Groupid",
                table: "Post",
                column: "Groupid");

            migrationBuilder.CreateIndex(
                name: "IX_Post_userId",
                table: "Post",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_RSVP_EventFK",
                table: "RSVP",
                column: "EventFK");

            migrationBuilder.CreateIndex(
                name: "IX_RSVP_UserFK",
                table: "RSVP",
                column: "UserFK");

            migrationBuilder.CreateIndex(
                name: "IX_TopicUser_UsersFollowid",
                table: "TopicUser",
                column: "UsersFollowid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comment");

            migrationBuilder.DropTable(
                name: "EventTopic");

            migrationBuilder.DropTable(
                name: "GroupMembers");

            migrationBuilder.DropTable(
                name: "InvitedGroups");

            migrationBuilder.DropTable(
                name: "InvitedUsers");

            migrationBuilder.DropTable(
                name: "RSVP");

            migrationBuilder.DropTable(
                name: "TopicUser");

            migrationBuilder.DropTable(
                name: "Post");

            migrationBuilder.DropTable(
                name: "Event");

            migrationBuilder.DropTable(
                name: "Topic");

            migrationBuilder.DropTable(
                name: "Group");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
