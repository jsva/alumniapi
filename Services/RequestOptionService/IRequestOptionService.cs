﻿using alumniAPI.Models.DTO.RequestOptions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Services.RequestOptionService
{
    public interface IRequestOptionService
    {
        /// <summary>
        /// Extracts a PostEventRequestOptionsDTO from a header object
        /// </summary>
        /// <param name="headers"></param>
        /// <returns></returns>
        public PostEventRequestOptionsDTO ExtractPostEventRequestsFromHeader(IHeaderDictionary headers);

        /// <summary>
        /// Extracts a GroupTopicRequestOptionsDTO from a header object
        /// if no such header was sent, it returns a new empty instance
        /// </summary>
        /// <param name="headers"></param>
        /// <returns></returns>
        public GroupTopicRequestOptionsDTO ExtractGroupTopicRequestsFromHeader(IHeaderDictionary headers);

        /// <summary>
        /// Checks if the Request option object has any valid options in it
        /// if it has returns true
        /// if all are empty returns false
        /// </summary>
        /// <param name="PostEventRequestOptionsDTO"></param>
        /// <returns></returns>
        public bool RequestOptionsExist(PostEventRequestOptionsDTO requestOptionsDto);

        /// <summary>
        /// Checks if the Request option object has any valid options in it
        /// if it has returns true
        /// if all are empty returns false
        /// </summary>
        /// <param name="PostEventRequestOptionsDTO"></param>
        /// <returns></returns>
        public bool RequestOptionsExist(GroupTopicRequestOptionsDTO requestOptionsDto);

    }
}
