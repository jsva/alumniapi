﻿using alumniAPI.Models.DTO.RequestOptions;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Services.RequestOptionService
{
    public class RequestOptionService : IRequestOptionService
    {
        public GroupTopicRequestOptionsDTO ExtractGroupTopicRequestsFromHeader(IHeaderDictionary headers)
        {
            if (headers.ContainsKey("requestoptions"))
            {
                var requestoptionString = headers["requestoptions"][0];
                var requestOptions = JsonConvert.DeserializeObject<GroupTopicRequestOptionsDTO>(requestoptionString);
                return requestOptions;
            }
            return new GroupTopicRequestOptionsDTO(); //return new empty object if none was found, should not apply filters
        }

        public PostEventRequestOptionsDTO ExtractPostEventRequestsFromHeader(IHeaderDictionary headers)
        {
            if (headers.ContainsKey("requestoptions"))
            {
                var requestoptionString = headers["requestoptions"][0];
                var requestOptions = JsonConvert.DeserializeObject<PostEventRequestOptionsDTO>(requestoptionString);
                return requestOptions;
            }
            return new PostEventRequestOptionsDTO(); //return new empty object if none was found, should not apply filters
        }

        public bool RequestOptionsExist(PostEventRequestOptionsDTO requestOptionsDto)
        {
            if (requestOptionsDto.groups.Any() || requestOptionsDto.topics.Any() || requestOptionsDto.limit > 0 || requestOptionsDto.offset > 0 ||
                (requestOptionsDto.order != "" && requestOptionsDto.order != "DESC") || requestOptionsDto.search != "")
            {
                return true;
            }
            return false;
        }

        public bool RequestOptionsExist(GroupTopicRequestOptionsDTO requestOptionsDto)
        {
            if ( requestOptionsDto.limit > 0 || requestOptionsDto.offset > 0 ||
                (requestOptionsDto.order != "" && requestOptionsDto.order != "DESC") || requestOptionsDto.search != "")
            {
                return true;
            }
            return false;
        }
    }
}
