﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumniAPI.Models;
using alumniAPI.Models.Domain;
using alumniAPI.Models.DTO.User;
using alumniAPI.Services.UserServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace alumniAPI.Services
{
    public class UserService : IUserService
    {
        private readonly AlumniDbContext _context;

        public UserService(AlumniDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<User>> GetAllUsersAsync()
        {
            return await _context.Users.Include(u => u.Groups)
                .ToListAsync();
        }

        public async Task<User> AddUserAsync(User user)
        {
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            return user;
        }

        public async Task<User> GetUserAsync(int id)
        {
            
            return await _context.Users.Include(u => u.Groups).Include(u => u.Events).Include(u => u.Topics).SingleOrDefaultAsync(u => u.id == id);
        }

       public async Task EditUser(UserEditDTO userEditDto)
        {
            User user = await GetUserAsync(userEditDto.Id);
            user.img = userEditDto.Img;
            user.status = userEditDto.Status;
            user.bio = userEditDto.Bio;
            user.funFact = userEditDto.FunFact;

            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();

        }

        public bool UserExists(int id)
        {
            return _context.Users.Any(u => u.id == id);
        }

        public async Task<bool> UserIsMemberOfGroup(int userid, int groupid)
        {

            //return await _context.Users.Where(u => u.id == userid).Include(u => u.Groups).Any(u => u.Groups.id == groupid);

            User user = await _context.Users.Include(u => u.Groups).SingleOrDefaultAsync(u => u.id == userid);
            foreach(Group group in user.Groups)
            {
                if (group.id == groupid)
                {
                    return true;
                }
            }

            return false;
        }

        public async Task<bool> UserIsMemberOfTopic(int userid, int topicid)
        {
            User user = await _context.Users.Include(u => u.Topics).SingleOrDefaultAsync(u => u.id == userid);
            foreach(Topic topic in user.Topics)
            {
                if(topic.id == topicid)
                {
                    return true;
                }
            }
            return false;
        }

        public async Task<bool> UserIsInvitedToEvent(int userid, int eventId)
        {
            Event ev = await _context.Events.Include(e => e.GroupsInvited).Include(e => e.TopicInvited).Include(e => e.UserInvited)
            .SingleOrDefaultAsync(e => e.id == eventId);
            //Creator of event is always counted as "invited" in this check
            if( ev.createdBy == userid)
            {
                return true;
            }
            User user = await _context.Users.Include(u => u.Events).SingleOrDefaultAsync(u => u.id == userid);
            // check if user is individually invited
            if (ev.UserInvited.Any(u => u.id == userid))
            {
                return true;
            }
            // check if user is member of any of the invited groups
            foreach(Group group in ev.GroupsInvited)
            {
                if (await UserIsMemberOfGroup(userid, group.id))
                {
                    return true;
                }
            }
            // check if any of the topics user  subscribe to is invited to 
            foreach(Topic topic in ev.TopicInvited)
            {
                if (await UserIsMemberOfTopic(userid, topic.id))
                {
                    return true;
                }
            }

            return false;

        }
    }
}
