﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alumniAPI.Models;
using alumniAPI.Models.DTO.User;

namespace alumniAPI.Services.UserServices
{
    public interface IUserService
    {
        /// <summary>
        /// Get all the users from the context as a Ienumberable of users
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<User>> GetAllUsersAsync();

        /// <summary>
        /// Adds a user object to the database
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public Task<User> AddUserAsync(User user);

        /// <summary>
        /// Retrieves a specific user from the context, by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<User> GetUserAsync(int id);

        /// <summary>
        /// Edits a user, by providing a new userobject with some or all of the properties changed
        /// </summary>
        /// <param name="userEditDto"></param>
        /// <returns></returns>
        public Task EditUser(UserEditDTO userEditDto);

        /// <summary>
        /// Returns a bool representing wether the user exists or not
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool UserExists(int id);

        /// <summary>
        /// Returns a bool representing if a given user is a member of a given group
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="groupid"></param>
        /// <returns></returns>
        public Task<bool> UserIsMemberOfGroup(int userid, int groupid);

        /// <summary>
        /// Returns a bool representing if a given user is a member of a given topic
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="topicid"></param>
        /// <returns></returns>
        public Task<bool> UserIsMemberOfTopic(int userid, int topicid);

        /// <summary>
        /// Returns a bool representing if a given user is invited to a given event
        /// will check for both if they are individually invited and also if they are invited through a groupmembership
        /// Will also return true if the user is the one who created the event
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public Task<bool> UserIsInvitedToEvent(int userid, int eventId);
    }
}
