﻿using alumniAPI.Models.Domain;
using alumniAPI.Models.DTO.Event;
using alumniAPI.Models.DTO.RequestOptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Services.EventService
{
   public interface IEventService
    {
        /// <summary>
        /// Retrieves all the events
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Event>> GetEvents();

        /// <summary>
        /// Gets all the events a user is invited to
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<IEnumerable<Event>> GetEventsForUser(int id);

        /// <summary>
        /// Adds a new event object to the database
        /// </summary>
        /// <param name="ev"></param>
        /// <returns></returns>
        public Task<Event> AddNewEventAsync(Event ev);

        /// <summary>
        /// Retrieves an event corresponding to the specified id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Event> GetEventById(int id);

        /// <summary>
        /// Returns a bool representing if the event exists or not
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public Task<bool> EventExists(int eventId);

        /// <summary>
        /// Updates the event with the new properties of a event object
        /// </summary>
        /// <param name="ev"></param>
        /// <returns></returns>
        public Task UpdateEvent(Event ev);

        /// <summary>
        /// Deletes the invite to a specified event for a specified group
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public Task DeleteEventGroupInvite(int eventId, int groupId);

        /// <summary>
        /// invites a specified group to a specified event
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public Task InviteGroupToEvent(int eventId, int groupId);

        /// <summary>
        /// Invites a specified user to a specified event
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Task InviteUserToEvent(int eventId, int userId);

        /// <summary>
        /// Deletes the invite of a specified user to a specified event
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Task DeleteEventUserInvite(int eventId, int userId);

        /// <summary>
        /// Invites a specified topic to a specified Event
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="topicId"></param>
        /// <returns></returns>
        public Task InviteTopicToEvent(int eventId, int topicId);

        /// <summary>
        /// Deletes the invitation of a specified topic to a specified event
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="topicId"></param>
        /// <returns></returns>
        public Task DeleteEventTopicInvite(int eventId, int topicId);

        /// <summary>
        /// Handles the requestoptions provided to a controller endpoint
        /// Will filter, search, order and paginate the events based on the requestoption
        /// </summary>
        /// <param name="events"></param>
        /// <param name="eventRequestOptionsDto"></param>
        /// <returns></returns>
        public IEnumerable<EventReadDTO> HandleRequestOptions(IEnumerable<EventReadDTO> events, PostEventRequestOptionsDTO eventRequestOptionsDto);

        /// <summary>
        /// Filters the events based on if the groups and topics specified by requestoptions
        /// are invited to an event.
        /// </summary>
        /// <param name="events"></param>
        /// <param name="groups"></param>
        /// <param name="topics"></param>
        /// <returns></returns>
        public IEnumerable<EventReadDTO> FilterEvents(IEnumerable<EventReadDTO> events, IEnumerable<int> groups, IEnumerable<int> topics);

        /// <summary>
        /// Searches the name and description of an event for a searchterm and returns all matches
        /// not case sensitive
        /// </summary>
        /// <param name="events"></param>
        /// <param name="searchterm"></param>
        /// <returns></returns>
        public IEnumerable<EventReadDTO> SearchEvents(IEnumerable<EventReadDTO> events, string searchterm);

        /// <summary>
        /// Order the events by the order specified
        /// if no valid order is provided, they are returned in the same order they came in
        /// </summary>
        /// <param name="events"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public IEnumerable<EventReadDTO> OrderEvents(IEnumerable<EventReadDTO> events, string order);



    }
}
