﻿using alumniAPI.Models;
using alumniAPI.Models.Domain;
using alumniAPI.Models.DTO.Event;
using alumniAPI.Models.DTO.RequestOptions;
using alumniAPI.Services.UserServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Services.EventService
{
    public class EventService : IEventService
    {

        private readonly AlumniDbContext _context;
        private readonly IUserService _userService;

        public EventService(AlumniDbContext context, IUserService userService)
        {
            _context = context;
            _userService = userService;
        }

        public async Task<IEnumerable<Event>> GetEvents()
        {

            return await _context.Events
                .Include(e => e.RSVPList).Include(e => e.GroupsInvited).Include(e => e.UserInvited).Include(e => e.TopicInvited)
                .ToListAsync();
        }

        public async Task<IEnumerable<Event>> GetEventsForUser(int id)
        {
            var allEvents = await GetEvents();
            List<Event> filteredEvents = new();
            foreach (Event ev in allEvents)
            {
                if (await _userService.UserIsInvitedToEvent(id, ev.id))
                {
                    filteredEvents.Add(ev);
                }
               
            }
            return filteredEvents;

        }

        public async Task<Event> AddNewEventAsync(Event ev)
        {
            ev.lastUpdated = DateTime.UtcNow;

            _context.Events.Add(ev);

            await _context.SaveChangesAsync();
            return (ev);
        }


        public async Task<Event> GetEventById(int id)
        {
            return await _context.Events
                .Include(e => e.UserInvited).Include(e => e.GroupsInvited).Include(e=> e.TopicInvited).Include(e => e.RSVPList)
                .FirstOrDefaultAsync(u => u.id == id);
        }

        public async Task UpdateEvent(Event ev)
        {
            ev.lastUpdated = DateTime.UtcNow;
            _context.Entry(ev).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<bool> EventExists(int eventId)
        {
            return await _context.Events.AnyAsync(e => e.id == eventId);
        }

        public async Task DeleteEventGroupInvite(int eventId, int groupId)
        {
            Group group = await _context.Groups.Include(g => g.EventInvites).SingleAsync(g => g.id == groupId);
            Event ev = await _context.Events.Include(e => e.GroupsInvited).SingleAsync(e => e.id == eventId);

            ev.GroupsInvited.Remove(group);

            _context.Entry(ev).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task InviteGroupToEvent(int eventId, int groupId)
        {
            Group group = await _context.Groups.Include(g => g.EventInvites).SingleAsync(g => g.id == groupId);
            Event ev = await _context.Events.Include(e => e.GroupsInvited).SingleAsync(e => e.id == eventId);

            ev.GroupsInvited.Add(group);

            _context.Entry(ev).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task InviteUserToEvent(int eventId, int userId)
        {
            User user = await _context.Users.FindAsync(userId);
            Event ev = await _context.Events.FindAsync(eventId);

            ev.UserInvited.Add(user);

            _context.Entry(ev).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteEventUserInvite(int eventId, int userId)
        {
            Event ev = await _context.Events.FindAsync(eventId);
            User user = await _context.Users.FindAsync(userId);

            ev.UserInvited.Remove(user);

            _context.Entry(ev).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task InviteTopicToEvent(int eventId, int topicId)
        {
            Topic topic = await _context.Topics.Include(t => t.EventInvited).SingleAsync(t => t.id == topicId);
            Event ev = await _context.Events.Include(e => e.GroupsInvited).SingleAsync(e => e.id == eventId);

            ev.TopicInvited.Add(topic);

            _context.Entry(ev).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteEventTopicInvite(int eventId, int topicId)
        {
            Topic topic = await _context.Topics.Include(t => t.EventInvited).SingleAsync(t => t.id == topicId);
            Event ev = await _context.Events.Include(e => e.GroupsInvited).SingleAsync(e => e.id == eventId);

            ev.TopicInvited.Remove(topic);

            _context.Entry(ev).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public IEnumerable<EventReadDTO> HandleRequestOptions(IEnumerable<EventReadDTO> events, PostEventRequestOptionsDTO eventRequestOptionsDto)
        {
            //Filter only wanted topics/groups
            events = FilterEvents(events, eventRequestOptionsDto.groups, eventRequestOptionsDto.topics);
            if (!events.Any()) //If no events remain, return the empty list now
            {
                return events;
            }
            //filter on text for name and description
            events = SearchEvents(events, eventRequestOptionsDto.search);
            if (!events.Any()) //If no events remain, return the empty list now
            {
                return events;
            }
            //Order
            events = OrderEvents(events, eventRequestOptionsDto.order);
            //Limit/Offset
            if (eventRequestOptionsDto.limit != null && eventRequestOptionsDto.limit > 0)
            {
                int limit = (int)eventRequestOptionsDto.limit;
                int offset = 0;
                if (eventRequestOptionsDto.offset != null)
                {
                    offset = (int)eventRequestOptionsDto.offset;
                }
                events = events.Skip(offset).Take(limit);
            }
            return events;
        }

        public IEnumerable<EventReadDTO> FilterEvents(IEnumerable<EventReadDTO> events, IEnumerable<int> groups, IEnumerable<int> topics)
        {
            List<EventReadDTO> filteredEvents = new();
            if (!(groups.Any() || topics.Any()) || //if both selections are empty don't filter
                (groups.Count() == 1 && groups.First() == 0 && topics.Count() == 1 && topics.First() == 0)) //or if they just contain the value 0 also don't filter
            {
                return events;
            }
            foreach (EventReadDTO ev in events)
            {
                List<int> sameGroups = groups.Where(g => ev.Group.Contains(g)).ToList(); //find all elements in both lists
                List<int> sameTopics = topics.Where(t => ev.Topics.Contains(t)).ToList();

                if (sameGroups.Any() || sameTopics.Any()) // if there were any same elements then this event should be listed
                {
                    filteredEvents.Add(ev);
                }
            }

            return filteredEvents;

        }

        public IEnumerable<EventReadDTO> SearchEvents(IEnumerable<EventReadDTO> events, string searchterm)
        {
            if (searchterm == "")
            {
                return events;
            }
            searchterm = searchterm.ToLower();
            List<EventReadDTO> matchingEvents = new();

            foreach(EventReadDTO ev in events)
            {
                if (ev.name.ToLower().Contains(searchterm) || ev.description.ToLower().Contains(searchterm))
                {
                    matchingEvents.Add(ev);
                }
            }

            return matchingEvents;
        }

        public IEnumerable<EventReadDTO> OrderEvents(IEnumerable<EventReadDTO> events, string order)
        {
            List<EventReadDTO> orderedEvents = order.ToUpper() switch
            {
                "ASC" => events.OrderBy(e => e.lastUpdated).ToList(),
                "ASC-LASTUPDATED" => events.OrderBy(e => e.lastUpdated).ToList(),
                "DESC" => events.OrderByDescending(e => e.lastUpdated).ToList(),
                "DESC-LASTUPDATED" => events.OrderByDescending(e => e.lastUpdated).ToList(),
                "ASC-NAME" => events.OrderBy(e => e.name).ToList(),
                "DESC-NAME" => events.OrderByDescending(e => e.name).ToList(),
                "ASC-START" => events.OrderBy(e => e.startTime).ToList(),
                "DESC-START" => events.OrderByDescending(e => e.startTime).ToList(),
                "ASC-END" => events.OrderBy(e => e.endTime).ToList(),
                "DESC-END" => events.OrderByDescending(e => e.endTime).ToList(),
                "ASC-CREATOR" => events.OrderBy(e => e.createdBy).ToList(),
                "DESC-CREATOR" => events.OrderByDescending(e => e.createdBy).ToList(),
                "ASC-DESCRIPTION" => events.OrderBy(e => e.description).ToList(),
                "DESC-DESCRIPTION" => events.OrderByDescending(e => e.description).ToList(),
                _ => events.ToList(),
            };
            return orderedEvents;
        }

        }
}
