﻿using alumniAPI.Models;
using alumniAPI.Models.DTO.Group;
using alumniAPI.Models.DTO.RequestOptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Services.GroupService
{
    public interface IGroupService
    {
        /// <summary>
        /// Gets all the groups
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Group>> GetAllGroupsAsync();

        /// <summary>
        /// Returns a bool representing if a user is a member of a group
        /// </summary>
        /// <param name="group"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsGroupMember(Group group, int id);

        /// <summary>
        /// Adds a Group object to the database asynchronously
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public Task<Group> AddGroupAsync(Group group);

        /// <summary>
        /// Returns a group specified by the id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Group> GetGroup(int id);

        /// <summary>
        /// Updates a group with the properties of a new Group object
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public Task UpdateGroupAsync(Group group);

        /// <summary>
        /// Returns a bool representing if the group exists or not
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<bool> GroupExists(int id);

        /// <summary>
        /// Handles the requestoptions, and searches, orders and paginates the groups
        /// </summary>
        /// <param name="groups"></param>
        /// <param name="postRequestOptionsDto"></param>
        /// <returns></returns>
        public IEnumerable<GroupReadDTO> HandleRequestOptions(IEnumerable<GroupReadDTO> groups, GroupTopicRequestOptionsDTO postRequestOptionsDto);

        /// <summary>
        /// Order the groups according to the order provided
        /// if no valid order is provided it will return them in the order they came in
        /// </summary>
        /// <param name="groups"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public IEnumerable<GroupReadDTO> OrderGroups(IEnumerable<GroupReadDTO> groups, string order);

        /// <summary>
        /// Searches through a collection of groups names and descriptions for a searchterm, only returns groups that matches
        /// not case sensitive, if empty string provided as search will return all groups that were passed in
        /// </summary>
        /// <param name="groups"></param>
        /// <param name="searchterm"></param>
        /// <returns></returns>
        public IEnumerable<GroupReadDTO> SearchGroups(IEnumerable<GroupReadDTO> groups, string searchterm);


    }
}
