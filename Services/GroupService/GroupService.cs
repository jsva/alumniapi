﻿using alumniAPI.Models;
using alumniAPI.Models.DTO.Group;
using alumniAPI.Models.DTO.RequestOptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Services.GroupService
{
    public class GroupService : IGroupService
    {
        private readonly AlumniDbContext _context;

        public GroupService(AlumniDbContext context)
        {
            _context = context;
        }


        public async Task<IEnumerable<Group>> GetAllGroupsAsync()
        {
            return await _context.Groups.Include(g => g.GroupMembers).Include(g => g.EventInvites).ToListAsync();
        }

        public bool IsGroupMember(Group group, int id)
        {
            
            return group.GroupMembers.Any(u => u.id == id);
        }

        public async Task<Group> AddGroupAsync(Group group)
        {
            _context.Groups.Add(group);
            await _context.SaveChangesAsync();
            return group;
        }

        public async Task<Group> GetGroup(int id)
        {
            return await _context.Groups.Include(g => g.GroupMembers).Include(g => g.EventInvites).FirstOrDefaultAsync(g => g.id == id);

        }

        public async Task UpdateGroupAsync(Group group)
        {
            _context.Entry(group).State = EntityState.Modified;

            await _context.SaveChangesAsync();
        }

        public async Task<bool> GroupExists(int id)
        {
            Group group = await _context.Groups.FindAsync(id);
            return group != null;
        }

        public IEnumerable<GroupReadDTO> HandleRequestOptions(IEnumerable<GroupReadDTO> groups, GroupTopicRequestOptionsDTO postRequestOptionsDto)
        {
            if (!groups.Any()) //If no posts remain, return the empty list now
            {
                return groups;
            }
            //filter on text
            groups = SearchGroups(groups, postRequestOptionsDto.search);
            if (!groups.Any()) //If no posts remain, return the empty list now
            {
                return groups;
            }
            //Order
            groups = OrderGroups(groups, postRequestOptionsDto.order);
            //Limit/Offset
            if (postRequestOptionsDto.limit != null && postRequestOptionsDto.limit > 0)
            {
                int limit = (int)postRequestOptionsDto.limit;
                int offset = 0;
                if (postRequestOptionsDto.offset != null)
                {
                    offset = (int)postRequestOptionsDto.offset;
                }
                groups = groups.Skip(offset).Take(limit);
            }
            return groups;
        }

        public IEnumerable<GroupReadDTO> OrderGroups(IEnumerable<GroupReadDTO> groups, string order)
        {
            List<GroupReadDTO> orderedPosts = order.ToUpper() switch
            {
                "ASC-NAME" => groups.OrderBy(g => g.name).ToList(),
                "DESC-NAME" => groups.OrderByDescending(g => g.name).ToList(),
                "ASC-DESCRIPTION" => groups.OrderBy(g => g.description).ToList(),
                "DESC-DESCRIPTION" => groups.OrderByDescending(g => g.description).ToList(),
                "ASC-PRIVATE" => groups.OrderBy(g => g.isPrivate).ToList(), 
                "DESC-PRIVATE" => groups.OrderByDescending(g => g.isPrivate).ToList(),
                "ASC-ID" => groups.OrderBy(g => g.id).ToList(),
                "DESC-ID" => groups.OrderByDescending(g => g.id).ToList(),
                "ASC-MEMBERS" => groups.OrderBy(g => g.Users.Count).ToList(),
                "DESC-MEMBERS" => groups.OrderByDescending(g => g.Users.Count).ToList(),
                
                _ => groups.ToList(),
            };
            return orderedPosts;
        }

        public IEnumerable<GroupReadDTO> SearchGroups(IEnumerable<GroupReadDTO> groups, string searchterm)
        {
            if(searchterm == "")
            {
                return groups;
            }
            searchterm = searchterm.ToLower();
            List<GroupReadDTO> matchingGroups = new();

            foreach (GroupReadDTO group in groups)
            {
                if (group.name.ToLower().Contains(searchterm) || group.description.ToLower().Contains(searchterm))
                {
                    matchingGroups.Add(group);
                }
            }
            return matchingGroups;
        }

    }
}
