﻿using alumniAPI.Models;
using alumniAPI.Models.Domain;
using alumniAPI.Models.DTO.RequestOptions;
using alumniAPI.Models.DTO.Topic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Services.TopicService
{
    public class TopicService : ITopicService
    {

        private readonly AlumniDbContext _context;

        public TopicService(AlumniDbContext context)
        {
            _context = context;
        }

        public async Task<Topic> AddTopicAsync(Topic topic)
        {
            _context.Topics.Add(topic);
            await _context.SaveChangesAsync();
            return topic;

        }

        public async Task<IEnumerable<Topic>> GetAllTopicsAsync()
        {
            return await  _context.Topics.Include(t => t.UsersFollow).Include(t => t.EventInvited).ToListAsync();
            
        }

        public async Task<Topic> GetTopicByIdAsync(int id)
        {
            return await _context.Topics.Include(t => t.UsersFollow).Include(t => t.EventInvited).SingleOrDefaultAsync(t => t.id == id);
        }

        public async Task InviteUserToTopicAsync(int topicId, int userId)
        {

            User user = _context.Users.Include(u => u.Topics).SingleOrDefault(u => u.id == userId);
            Topic topic = _context.Topics.Include(t => t.UsersFollow).SingleOrDefault(t => t.id == topicId);
            topic.UsersFollow.Add(user);
            _context.Entry(topic).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<bool> TopicExistsAsync(int topicId)
        {
            var topic = await _context.Topics.FindAsync(topicId);
            return topic != null;
        }

        public IEnumerable<TopicReadDTO> HandleRequestOptions(IEnumerable<TopicReadDTO> topics, GroupTopicRequestOptionsDTO postRequestOptionsDto)
        {
            if (!topics.Any()) //If no posts remain, return the empty list now
            {
                return topics;
            }
            //filter on text
            topics = SearchTopics(topics, postRequestOptionsDto.search);
            if (!topics.Any()) //If no posts remain, return the empty list now
            {
                return topics;
            }
            //Order
            topics = OrderTopics(topics, postRequestOptionsDto.order);
            //Limit/Offset
            if (postRequestOptionsDto.limit != null && postRequestOptionsDto.limit > 0)
            {
                int limit = (int)postRequestOptionsDto.limit;
                int offset = 0;
                if (postRequestOptionsDto.offset != null)
                {
                    offset = (int)postRequestOptionsDto.offset;
                }
                topics = topics.Skip(offset).Take(limit);
            }
            return topics;
        }

        public IEnumerable<TopicReadDTO> OrderTopics(IEnumerable<TopicReadDTO> topics, string order)
        {
            List<TopicReadDTO> orderedPosts = order.ToUpper() switch
            {
                "ASC-NAME" => topics.OrderBy(t => t.name).ToList(),
                "DESC-NAME" => topics.OrderByDescending(t => t.name).ToList(),
                "ASC-DESCRIPTION" => topics.OrderBy(t => t.description).ToList(),
                "DESC-DESCRIPTION" => topics.OrderByDescending(t => t.description).ToList(),
                "ASC-ID" => topics.OrderBy(t => t.id).ToList(),
                "DESC-ID" => topics.OrderByDescending(t => t.id).ToList(),
                "ASC-MEMBERS" => topics.OrderBy(t => t.UsersFollow.Count).ToList(),
                "DESC-MEMBERS" => topics.OrderByDescending(t => t.UsersFollow.Count).ToList(),
                _ => topics.ToList(),
            };
            return orderedPosts;
        }

        public IEnumerable<TopicReadDTO> SearchTopics(IEnumerable<TopicReadDTO> topics, string searchterm)
        {
            if (searchterm == "")
            {
                return topics;
            }
            searchterm = searchterm.ToLower();
            List<TopicReadDTO> matchingTopics = new();

            foreach (TopicReadDTO topic in topics)
            {
                if (topic.name.ToLower().Contains(searchterm) || topic.description.ToLower().Contains(searchterm))
                {
                    matchingTopics.Add(topic);
                }
            }
            return matchingTopics;
        }
    }
}
