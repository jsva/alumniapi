﻿
using alumniAPI.Models.Domain;
using alumniAPI.Models.DTO.RequestOptions;
using alumniAPI.Models.DTO.Topic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Services.TopicService
{
    public interface ITopicService
    {

        /// <summary>
        /// Returns all topics 
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Topic>> GetAllTopicsAsync();
        /// <summary>
        /// Returns a given topic specified by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Topic> GetTopicByIdAsync(int id);

        /// <summary>
        /// Adds a Topic object to the database asynchrnously
        /// </summary>
        /// <param name="topic"></param>
        /// <returns></returns>
        public Task<Topic> AddTopicAsync(Topic topic);

        /// <summary>
        /// Invites a user specified by Id to a Topic specified by id
        /// </summary>
        /// <param name="topicId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Task InviteUserToTopicAsync(int topicId, int userId);

        /// <summary>
        /// Returns a bool representing wether the specified topic exists or not
        /// </summary>
        /// <param name="topicId"></param>
        /// <returns></returns>
        public Task<bool> TopicExistsAsync(int topicId);

        /// <summary>
        /// Handles the requestoptions, and searches, orders and paginates the topics
        /// </summary>
        /// <param name="topics"></param>
        /// <param name="postRequestOptionsDto"></param>
        /// <returns></returns>
        public IEnumerable<TopicReadDTO> HandleRequestOptions(IEnumerable<TopicReadDTO> topics, GroupTopicRequestOptionsDTO postRequestOptionsDto);

        /// <summary>
        /// Order the topics according to the order provided
        /// if no valid order is provided it will return them in the order they came in
        /// </summary>
        /// <param name="topics"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public IEnumerable<TopicReadDTO> OrderTopics(IEnumerable<TopicReadDTO> topics, string order);

        /// <summary>
        /// Searches through a collection of topics names and descriptions for a searchterm, only returns topics that matches
        /// not case sensitive, if empty string provided as search will return all topics that were passed in
        /// </summary>
        /// <param name="topics"></param>
        /// <param name="searchterm"></param>
        /// <returns></returns>
        public IEnumerable<TopicReadDTO> SearchTopics(IEnumerable<TopicReadDTO> topics, string searchterm);







    }
}
