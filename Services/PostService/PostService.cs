﻿using alumniAPI.Enums;
using alumniAPI.Models;
using alumniAPI.Models.Domain;
using alumniAPI.Models.DTO.Post;
using alumniAPI.Models.DTO.Post.Comment;
using alumniAPI.Models.DTO.RequestOptions;
using alumniAPI.Models.DTO.User;
using alumniAPI.Services.UserServices;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Services.PostService
{
    public class PostService : IPostService
    {
        private readonly AlumniDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        public PostService(AlumniDbContext context, IMapper mapper, IUserService userService)
        {
            _context = context;
            _mapper = mapper;
            _userService = userService;
        }
        public async Task<Post> AddPostAsync(Post post)
        {
            post.lastUpdated = DateTime.UtcNow;
            _context.Posts.Add(post);
            await _context.SaveChangesAsync();
            return post;
        }

        public async Task<CommentReadDTO> AssembleComment(Comment comment)
        {
            CommentReadDTO commentReadDto = _mapper.Map<CommentReadDTO>(comment);
            commentReadDto.comments = await GetChildComments(comment.id);
            
            return commentReadDto;

        }

        public async Task<PostReadDTO> AssemblePost(Post post)
        {
            PostReadDTO postReadDto = _mapper.Map<PostReadDTO>(post);
            postReadDto.Comments = await GetPostComments(post.id);
            return postReadDto;
        }

        public async Task EditPostAsync(PostEditDTO postEditDto)
        {
            //Workaround for error that occured where mapper produced error
            //The error is that you are not allowed two objects with same id property
            Post post = await _context.Posts.FindAsync(postEditDto.id);
            post.lastUpdated = DateTime.UtcNow;
            if (postEditDto.title != null) //try to protect from deleting title as should always have title
            {
                post.title = postEditDto.title;
            }
            post.text = postEditDto.text;
            _context.Entry(post).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<PostReadDTO>> GetPostsOfTypeByTypesIdAsync(int id, PostTypes postType)
        {
            List<Post> matchingPosts = postType switch
            {
                PostTypes.ALL => await _context.Posts.Include(p => p.user).OrderByDescending(p => p.lastUpdated).ToListAsync(),
                PostTypes.EVENT => await _context.Posts.Include(p => p.user)
                    .Where(p => p.targetEventId == id).OrderByDescending(p => p.lastUpdated).ToListAsync(),
                PostTypes.GROUP => await _context.Posts.Include(p => p.user)
                    .Where(p => p.targetGroupId == id).OrderByDescending(p => p.lastUpdated).ToListAsync(),
                PostTypes.TOPIC => await _context.Posts.Include(p => p.user)
                    .Where(p => p.targetTopicId == id).OrderByDescending(p => p.lastUpdated).ToListAsync(),
                PostTypes.USER => await _context.Posts.Include(p => p.user)
                    .Where(p => p.targetUserId == id).OrderByDescending(p => p.lastUpdated).ToListAsync(),
                _ => new(),
            };
            List<PostReadDTO> posts = new();

            foreach (Post post in matchingPosts)
            {
                posts.Add(await AssemblePost(post));
            }
            return posts;
        }

        public async Task<IEnumerable<PostReadDTO>> GetAllPostsAsync()
        {
            List<PostReadDTO> posts = new();
            foreach (Post post in await _context.Posts.Include(p => p.user).ToListAsync())
            {
                posts.Add(await AssemblePost(post));
            }
            return posts;
        }

        public async Task<IEnumerable<CommentReadDTO>> GetChildComments(int id)
        {
            List<CommentReadDTO> comments = new();
            foreach (Comment comment in await _context.Comments.Include(c => c.user).Where(c => c.parentCommentId == id).ToListAsync())
            {
                comments.Add(await AssembleComment(comment));
            }
            return comments;
        }

        public async Task<PostReadDTO> GetPostAsync(int id)
        {
            return await AssemblePost(await _context.Posts.Include(p => p.user).Where(p => p.id == id).FirstOrDefaultAsync());
        }

        public async Task<IEnumerable<CommentReadDTO>> GetPostComments(int id)
        {
            List<CommentReadDTO> comments = new();
            foreach (Comment comment in await _context.Comments.Include(c => c.user).Where(c => c.postId == id).ToListAsync())
            {
                if (comment.parentCommentId > 0)
                {
                    continue;
                }
                comments.Add(await AssembleComment(comment));
            }
            return comments;
        }

        public async Task<bool> PostExistsAsync(int id)
        {
            return await _context.Posts.FindAsync(id) != null;
        }

        public async Task<IEnumerable<PostReadDTO>> GetAllPostsToUserFromUser(int userId, int senderId)
        {
            List<Post> matchingPosts = await _context.Posts.Include(p => p.user).Where(p => p.targetUserId == userId && p.userId == senderId).ToListAsync();
            List<PostReadDTO> posts = new();
            foreach (Post post in matchingPosts)
            {
                posts.Add(await AssemblePost(post));
            }
            return posts;
        }

        public async Task<Comment> AddCommentAsync(Comment comment)
        {
            comment.lastUpdated = DateTime.UtcNow;
            _context.Comments.Add(comment);
            await _context.SaveChangesAsync();

            return comment;
        }

        public async Task EditCommentAsync(CommentEditDTO commentEditDto)
        {
            //Do the update in this bad way here too due to errors from mapping
            Comment comment = await _context.Comments.FindAsync(commentEditDto.id);
            comment.lastUpdated = DateTime.UtcNow;
            comment.text = commentEditDto.text;
            _context.Entry(comment).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<CommentReadDTO> GetCommentAsync(int id)
        {
            return await AssembleComment(await _context.Comments.Include(c => c.user).Where(c => c.id == id).FirstOrDefaultAsync());
        }

        public async Task<bool> CommentExistsAsync(int id)
        {
            return await _context.Comments.FindAsync(id) != null;
        }

        public async Task<IEnumerable<PostReadDTO>> GetAllPostsForUser(int id)
        {
            User user = await _context.Users.Include(u => u.Groups).Include(u => u.Topics).SingleOrDefaultAsync(u => u.id == id);
            List<PostReadDTO> posts = new();
            foreach (Group group in user.Groups)
            {
                posts.AddRange(await GetPostsOfTypeByTypesIdAsync(group.id, PostTypes.GROUP));
            }
            foreach (Topic topic in user.Topics)
            {
                posts.AddRange(await GetPostsOfTypeByTypesIdAsync(topic.id, PostTypes.TOPIC));
            }
            return posts.OrderByDescending(p => p.lastUpdated);
        }

        public IEnumerable<PostReadDTO> HandleRequestOptions(IEnumerable<PostReadDTO> posts, PostEventRequestOptionsDTO postRequestOptionsDto)
        {
            //Filter only wanted topics/groups/events
            posts = FilterPosts(posts, postRequestOptionsDto.groups, postRequestOptionsDto.topics);
            if (!posts.Any()) //If no posts remain, return the empty list now
            {
                return posts;
            }
            //filter on text
            posts = SearchPostsAndComments(posts, postRequestOptionsDto.search);
            if (!posts.Any()) //If no posts remain, return the empty list now
            {
                return posts;
            }
            //Order
            posts = OrderPosts(posts, postRequestOptionsDto.order);
            //Limit/Offset
            if (postRequestOptionsDto.limit != null && postRequestOptionsDto.limit > 0)
            {
                int limit = (int)postRequestOptionsDto.limit;
                int offset = 0;
                if (postRequestOptionsDto.offset != null)
                {
                    offset = (int)postRequestOptionsDto.offset;
                }
                posts = posts.Skip(offset).Take(limit);
            }
            return posts;
        }

        public IEnumerable<PostReadDTO> HandleRequestOptions(IEnumerable<PostReadDTO> posts, GroupTopicRequestOptionsDTO postRequestOptionsDto)
        {
            if (!posts.Any()) //If no posts remain, return the empty list now
            {
                return posts;
            }
            //filter on text
            posts = SearchPostsAndComments(posts, postRequestOptionsDto.search);
            if (!posts.Any()) //If no posts remain, return the empty list now
            {
                return posts;
            }
            //Order
            posts = OrderPosts(posts, postRequestOptionsDto.order);
            //Limit/Offset
            if (postRequestOptionsDto.limit != null && postRequestOptionsDto.limit > 0)
            {
                int limit = (int)postRequestOptionsDto.limit;
                int offset = 0;
                if (postRequestOptionsDto.offset != null)
                {
                    offset = (int)postRequestOptionsDto.offset;
                }
                posts = posts.Skip(offset).Take(limit);
            }
            return posts;
        }

        public IEnumerable<PostReadDTO> FilterPosts(IEnumerable<PostReadDTO> posts, IEnumerable<int> groups, IEnumerable<int> topics)
        {
            List<PostReadDTO> filteredPosts = new();
            if (!(groups.Any() || topics.Any()) || //if both selections are empty don't filter
                (groups.Count() == 1 && groups.First() == 0 && topics.Count() == 1 && topics.First() == 0)) //or if they just contain the value 0 also don't filter
            {
                return posts;
            }
            foreach (PostReadDTO post in posts)
            {
                if (groups.Contains((int)post.targetGroupId) || topics.Contains((int)post.targetTopicId))
                {
                    filteredPosts.Add(post);
                }
            }

            return filteredPosts;

        }

        public IEnumerable<PostReadDTO> OrderPosts(IEnumerable<PostReadDTO> posts, string order)
        {
            List<PostReadDTO> orderedPosts = order.ToUpper() switch
            {
                "ASC" => posts.OrderBy(p => p.lastUpdated).ToList(),
                "ASC-LASTUPDATED" => posts.OrderBy(p => p.lastUpdated).ToList(),
                "DESC" => posts.OrderByDescending(p => p.lastUpdated).ToList(),
                "DESC-LASTUPDATED" => posts.OrderByDescending(p => p.lastUpdated).ToList(),
                "ASC-TITLE" => posts.OrderBy(p => p.title).ToList(),
                "DESC-TITLE" => posts.OrderByDescending(p => p.title).ToList(),
                "ASC-GROUP" => posts.OrderBy(p => p.targetGroupId).ToList(),
                "DESC-GROUP" => posts.OrderByDescending(p => p.targetGroupId).ToList(),
                "ASC-TOPIC" => posts.OrderBy(p => p.targetTopicId).ToList(),
                "DESC-TOPIC" => posts.OrderByDescending(p => p.targetTopicId).ToList(),
                "ASC-USER" => posts.OrderBy(p => p.targetUSerId).ToList(),
                "DESC-USER" => posts.OrderByDescending(p => p.targetUSerId).ToList(),
                "ASC-USERNAME" => posts.OrderBy(p => p.user.username).ToList(),
                "DESC-USERNAME" => posts.OrderByDescending(p => p.user.username).ToList(),
                _ => posts.ToList(),
            };
            return orderedPosts;
        }

        public async Task<bool> UserAllowedToPost(int id, Post post)
        {
            if (post.targetUserId > 0)
            {
                return true; //always allowed to send posts to users
            }
            else if (post.targetGroupId > 0)
            {
                return await _userService.UserIsMemberOfGroup(id, post.targetGroupId);
            }
            else if (post.targetEventId > 0)
            {
                return await _userService.UserIsInvitedToEvent(id, post.targetEventId);
            }
            else if (post.targetTopicId > 0)
            {
                return await _userService.UserIsMemberOfTopic(id, (int)post.targetTopicId);
            }
            return false;
        }
        public async Task<bool> UserAllowedToPost(int id, int postid)
        {
            Post post = await _context.Posts.FindAsync(postid);
            if (post.targetUserId > 0)
            {
                return true; //always allowed to send posts to users
            }
            else if (post.targetGroupId > 0)
            {
                return await _userService.UserIsMemberOfGroup(id, post.targetGroupId);
            }
            else if (post.targetEventId > 0)
            {
                return await _userService.UserIsInvitedToEvent(id, post.targetEventId);
            }
            else if (post.targetTopicId > 0)
            {
                return await _userService.UserIsMemberOfTopic(id, (int)post.targetTopicId);
            }
            return false;
        }

        public IEnumerable<PostReadDTO> SearchPostsAndComments(IEnumerable<PostReadDTO> posts, string searchterm)
        {
            searchterm = searchterm.ToLower();
            List<PostReadDTO> matchingPosts = new();
            foreach (PostReadDTO post in posts)
            {
                if (post.title.ToLower().Contains(searchterm) || post.text.ToLower().Contains(searchterm))
                {
                    matchingPosts.Add(post);
                    continue;
                }
                foreach (CommentReadDTO comment in post.Comments)
                {
                    if (SearchComment(comment, searchterm))
                    {
                        matchingPosts.Add(post);
                        break;
                    }
                }
            }
            return matchingPosts;
        }

        public bool SearchComment(CommentReadDTO comment, string searchterm)
        {
            if (comment.text.ToLower().Contains(searchterm))
            {
                return true;
            }
            if (comment.comments.Any())
            {
                foreach (CommentReadDTO childComment in comment.comments)
                {
                    if (SearchComment(childComment, searchterm))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
