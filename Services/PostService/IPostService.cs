﻿using alumniAPI.Enums;
using alumniAPI.Models.Domain;
using alumniAPI.Models.DTO.Post;
using alumniAPI.Models.DTO.Post.Comment;
using alumniAPI.Models.DTO.RequestOptions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Services.PostService
{
    public interface IPostService
    {
        /// <summary>
        /// Get a post from the database
        /// Async method
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<PostReadDTO> GetPostAsync(int id);
        /// <summary>
        /// Get a comment from the database
        /// Async method
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<CommentReadDTO> GetCommentAsync(int id);
        /// <summary>
        /// Check if a post exists
        /// Async method
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<bool> PostExistsAsync(int id);
        /// <summary>
        /// Check if Comment exists
        /// Async method
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<bool> CommentExistsAsync(int id);
        /// <summary>
        /// Get all the posts in the database
        /// Async method
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<PostReadDTO>> GetAllPostsAsync();
        /// <summary>
        /// Gets all the posts that have the same type that you specify
        /// and matches the id you have provided
        /// Async method
        /// </summary>
        /// <param name="id"></param>
        /// <param name="postType"></param>
        /// <returns></returns>
        public Task<IEnumerable<PostReadDTO>> GetPostsOfTypeByTypesIdAsync(int id, PostTypes postType);
        /// <summary>
        /// Gets all posts from a user to another user
        /// Async method
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="senderId"></param>
        /// <returns></returns>
        public Task<IEnumerable<PostReadDTO>> GetAllPostsToUserFromUser(int userId, int senderId);
        /// <summary>
        /// Helper method that "assembles" a post object
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public Task<PostReadDTO> AssemblePost(Post post);
        /// <summary>
        /// Adds a post to the database
        /// Async method
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public Task<Post> AddPostAsync(Post post);
        /// <summary>
        /// Adds a comment to the database
        /// Async method
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public Task<Comment> AddCommentAsync(Comment comment);
        /// <summary>
        /// Edits a post in the database
        /// Async method
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public Task EditPostAsync(PostEditDTO postEditDto);
        /// <summary>
        /// Edits a comment in the datab ase
        /// Async method
        /// </summary>
        /// <param name="commment"></param>
        /// <returns></returns>
        public Task EditCommentAsync(CommentEditDTO commmentEditDto);
        /// <summary>
        /// Get all the comments belonging to a specified post
        /// Async method
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<IEnumerable<CommentReadDTO>> GetPostComments(int id);
        /// <summary>
        /// Gets all the comments that are responses to the specified comment
        /// Async method
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<IEnumerable<CommentReadDTO>> GetChildComments(int id);
        /// <summary>
        /// Helper method to "assemble" a comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public Task<CommentReadDTO> AssembleComment(Comment comment);
        /// <summary>
        /// Gets all the posts a user should be able to see
        /// based on the users membership in groups and topics
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<IEnumerable<PostReadDTO>> GetAllPostsForUser(int id);

        /// <summary>
        /// Handles the request-options passed to the controller
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PostReadDTO> HandleRequestOptions(IEnumerable<PostReadDTO> posts, PostEventRequestOptionsDTO postRequestOptionsDto);

        /// <summary>
        /// Handles the request-options passed to the controller
        /// Does not handle filtering of posts to certain groups and/or topics
        /// </summary>
        /// <param name="posts"></param>
        /// <param name="postRequestOptionsDto"></param>
        /// <returns></returns>
        public IEnumerable<PostReadDTO> HandleRequestOptions(IEnumerable<PostReadDTO> posts, GroupTopicRequestOptionsDTO postRequestOptionsDto);


        /// <summary>
        /// Filters posts to only include posts to the given groups, topics and/or events
        /// </summary>
        /// <param name="posts"></param>
        /// <param name="postTypes"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IEnumerable<PostReadDTO> FilterPosts(IEnumerable<PostReadDTO> posts, IEnumerable<int> groups, IEnumerable<int> topics);

        /// <summary>
        /// Order the posts according to the order from a postRequestoptionsDTO
        /// </summary>
        /// <param name="posts"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public IEnumerable<PostReadDTO> OrderPosts(IEnumerable<PostReadDTO> posts, string order);
        /// <summary>
        /// Returns a bool for wether the user has the proper memberships to post this post to its intended target
        /// </summary>
        /// <param name="id"></param>
        /// <param name="post"></param>
        /// <returns></returns>
        public Task<bool> UserAllowedToPost(int id, Post post);

        /// <summary>
        /// Returns a bool for wether the user has proper membership to post to this target
        /// </summary>
        /// <param name="id"></param>
        /// <param name="postid"></param>
        /// <returns></returns>
        public Task<bool> UserAllowedToPost(int id, int postid);


        /// <summary>
        /// Searches through all posts passed to it for a searchterm
        /// Checks the title and text for the searchterm, if not found it will also check all the comments
        /// if searchterm is found the post is added to a list which is returned after all psots has been searched through
        /// </summary>
        /// <param name="posts"></param>
        /// <param name="searchterm"></param>
        /// <returns></returns>
        public IEnumerable<PostReadDTO> SearchPostsAndComments(IEnumerable<PostReadDTO> posts, string searchterm);

        /// <summary>
        /// Searches a comments text for the seaarch term.
        /// If not found and the comments have child-comments it will recursively call this method on its child-comments
        /// until one matches the search term or all children have been searched
        /// </summary>
        /// <param name="comment"></param>
        /// <param name="searchterm"></param>
        /// <returns></returns>
        public bool SearchComment(CommentReadDTO comment, string searchterm);

    }
}
