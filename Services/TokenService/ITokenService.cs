﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Services.TokenService
{
    public interface ITokenService
    {
        /// <summary>
        /// Returns the id that belongs to the user with the provided username
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public Task<int> GetUserIdFromUsernameAsync(string username);

        /// <summary>
        /// Registers a new user to the database, with the username provided
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public Task RegisterUsernameAsync(string username);

        /// <summary>
        /// Takes an authentication token and extracts the username from it
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public string ExtractUsernameFromToken(string token);
    }
}
