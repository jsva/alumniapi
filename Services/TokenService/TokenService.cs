﻿using alumniAPI.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace alumniAPI.Services.TokenService
{
    public class TokenService : ITokenService
    {
        private readonly AlumniDbContext _context;

        public TokenService(AlumniDbContext context)
        {
            _context = context;
        }

        public string ExtractUsernameFromToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(token);
            return jwtSecurityToken.Payload["preferred_username"].ToString();
        }

        public async Task<int> GetUserIdFromUsernameAsync(string username)
        {
            User user =  _context.Users.SingleOrDefault<User>(u => u.username == username);
            if (user == null)
            {
                await RegisterUsernameAsync(username);
                user = _context.Users.SingleOrDefault<User>(u => u.username == username);
            }
            return user.id;
        }

        public async Task RegisterUsernameAsync(string username)
        {
            User user = new();
            user.username = username;
            user.status = "";
            user.img = "";
            user.bio = "";
            user.funFact = "";
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
        }
    }
}
